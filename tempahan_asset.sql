-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2019 at 07:53 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tempahan_asset`
--

CREATE TABLE `tempahan_asset` (
  `id` int(3) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `no_matric` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `no_tel` varchar(255) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `asset_id` int(3) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempahan_asset`
--

INSERT INTO `tempahan_asset` (`id`, `user_name`, `no_matric`, `user_email`, `no_tel`, `purpose`, `start_date`, `end_date`, `asset_id`, `status`) VALUES
(1, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'wervwev', '2019-12-13', '2019-12-19', 8, 'Pending'),
(2, 'user', 'user1', 'darwish791@gmail.com', '343423', 'fssdfsdf', '2019-12-25', '2019-12-20', 8, 'Pending'),
(3, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'wvcwev', '2019-12-13', '2019-12-21', 8, 'Pending'),
(4, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'tangkap gambar', '2019-12-17', '2019-12-26', 9, 'Pending'),
(5, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'test tempah', '2019-12-20', '2019-12-12', 14, 'Pending'),
(6, 'user', 'user2', 'darwish791@gmail.com', '1561165515', 'nak test tempah', '2019-12-17', '2019-12-20', 11, 'Pending');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tempahan_asset`
--
ALTER TABLE `tempahan_asset`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tempahan_asset`
--
ALTER TABLE `tempahan_asset`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
