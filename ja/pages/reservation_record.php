<?php include "../includes/admin_header.php"; ?>

<?php

$query_reservation = "SELECT * FROM tempahan WHERE status != 'Pending' AND WEEKDAY(start_date) BETWEEN 0 AND 4";
$result_reservation = mysqli_query($connection, $query_reservation);

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

				<div class="row">

				<div class="col-xl-3 col-md-6 mb-4">
					<a href="reservation_record_sem.php">
						<div class="card border-left-success shadow h-100 py-2">
							<div class="card-body">
								<div class="row no-gutters align-items-center">
									<div class="col mr-2">
										<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Rekod Tempahan Semester</div>
										<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
									</div>
									<div class="col-auto">
										<i class="fas fa-calendar-check fa-2x text-gray-300"></i>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
				</div>
				<br>

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Rekod Tempahan Mingguan</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
											<th>Bil</th>
                      <th>Nama Pelajar</th>
                      <th>No Matrik</th>
                      <th>Ruang</th>
                      <th>Tujuan</th>
                      <th>No Tel</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Masa Mula</th>
                      <th>Masa Akhir</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
											<th>Bil</th>
                      <th>Nama Pelajar</th>
                      <th>No Matrik</th>
                      <th>Ruang</th>
                      <th>Tujuan</th>
                      <th>No Tel</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Masa Mula</th>
                      <th>Masa Akhir</th>
                      <th>Status</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_reservation = mysqli_fetch_assoc($result_reservation)) : ?>

											<tr>
											<?php

													$room_id = $row_reservation['room_id'];
													$query_room = "SELECT * FROM rooms WHERE id = $room_id";
													$result_room = mysqli_query($connection, $query_room);
													$row_room = mysqli_fetch_array($result_room);
											?>

												<td><?php echo $i; ?></td>
												<td><?php echo $row_reservation['name']; ?></td>
												<td><?php echo $row_reservation['user_matric_num']; ?></td>
												<td><?php echo $row_room['name']; ?></td>
												<td><?php echo $row_reservation['purposes']; ?></td>
												<td><?php echo $row_reservation['no_tel']; ?></td>
												<td><?php echo $row_reservation['start_date']; ?></td>
												<td><?php echo $row_reservation['end_date']; ?></td>
												<td><?php echo $row_reservation['start_time']; ?></td>
												<td><?php echo $row_reservation['end_time']; ?></td>
												<td><?php echo $row_reservation['status']; ?></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->