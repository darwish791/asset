<?php include "../includes/admin_header.php"; ?>

<?php

$query_reservation = "SELECT * FROM tempahan_asset WHERE status != 'Pending' AND WEEKDAY(start_date) BETWEEN 0 AND 4";
$result_reservation = mysqli_query($connection, $query_reservation);

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Rekod Tempahan Asset</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
											<th>Bil</th>
                      <th>Nama Pelajar</th>
                      <th>No Matrik</th>
                      <th>Asset</th>
                      <th>Tujuan</th>
                      <th>No Tel</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Status</th>
                      <th>Padam</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
											<th>Bil</th>
                      <th>Nama Pelajar</th>
                      <th>No Matrik</th>
                      <th>Asset</th>
                      <th>Tujuan</th>
                      <th>No Tel</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Status</th>
                      <th>Padam</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_reservation = mysqli_fetch_assoc($result_reservation)) : ?>

											<tr>
											<?php

													$asset_id = $row_reservation['asset_id'];
													$query_asset = "SELECT * FROM assets WHERE id = $asset_id";
													$result_asset = mysqli_query($connection, $query_asset);
													$row_asset = mysqli_fetch_array($result_asset);
											?>

												<td><?php echo $i; ?></td>
												<td><?php echo $row_reservation['user_name']; ?></td>
												<td><?php echo $row_reservation['no_matric']; ?></td>
												<td><?php echo $row_asset['asset_name']; ?></td>
												<td><?php echo $row_reservation['purpose']; ?></td>
												<td><?php echo $row_reservation['no_tel']; ?></td>
												<td><?php echo $row_reservation['start_date']; ?></td>
												<td><?php echo $row_reservation['end_date']; ?></td>
												<td><?php echo $row_reservation['status']; ?></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->