<?php include "../includes/admin_header.php"; ?>

<?php 
										
	$query_room = "SELECT * FROM rooms WHERE room_level = 'Aras Bawah'";
	$result_room = mysqli_query($connection, $query_room);

	if (isset($_GET['aras'])) {
		
		$aras = $_GET['aras'];
		if ($aras == 0) { $aras = "Aras Bawah"; }

		$query_room = "SELECT * FROM rooms WHERE room_level = '$aras'";
		$result_room = mysqli_query($connection, $query_room);
	}

	if(isset($_GET['delete'])){

		$room_id = $_GET['delete'];
		$query_to_delete = "DELETE FROM rooms WHERE id = $room_id";
		$result_to_delete = mysqli_query($connection, $query_to_delete);
		header("Location: room_list.php");
	}
?>



<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Senarai Bilik</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

					<!-- Content Row -->
          <div class="row">

            <!-- Aras 1 -->
            <div class="col-xl-3 col-md-6 mb-4">
							<a href="room_list.php?aras=0">
								<div class="card border-left-primary shadow h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Aras Bawah</div>
												<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div> -->
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
            </div>

            <!-- Aras 2 -->
            <div class="col-xl-3 col-md-6 mb-4">
							<a href="room_list.php?aras=1">
								<div class="card border-left-success shadow h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Aras 1</div>
												<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
            </div>

            <!-- Aras 3 -->
            <div class="col-xl-3 col-md-6 mb-4">
							<a href="room_list.php?aras=2">
								<div class="card border-left-info shadow h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Aras 2</div>
												<div class="row no-gutters align-items-center">
													<!-- <div class="col-auto">
														<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
													</div> -->
													<!-- <div class="col">
														<div class="progress progress-sm mr-2">
															<div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div> -->
												</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
            </div>

            <!-- G3 -->
            <div class="col-xl-3 col-md-6 mb-4">
							<a href="room_list.php?aras=3">
								<div class="card border-left-warning shadow h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Aras 3</div>
												<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">18</div> -->
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</a>
          </div>




          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>Kapasiti</th>
                      <th>Aras</th>
                      <th>Deskripsi</th>
                      <th>Jadual</th>
                      <th>Jadual</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>Kapasiti</th>
                      <th>Aras</th>
                      <th>Deskripsi</th>
                      <th>Jadual</th>
                      <th>Jadual</th>
                      <th>Action</th>
                    </tr>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_room = mysqli_fetch_assoc($result_room)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_room['name'];  ?></td>
												<td><?php echo $row_room['capacity']; ?></td>
												<td><?php echo $row_room['room_level']; ?></td>
												<td><?php echo $row_room['room_description']; ?></td>
												<td><a href="room_schedule.php?id=<?php echo $row_room['id']; ?>" class="btn btn-primary">1 Sem</a></td>
												<td><a href="room_schedule_week.php?id=<?php echo $row_room['id']; ?>" class="btn btn-primary">Mingguan</a></td>
												<td>
													<center>
														<a href="update_room.php?id=<?php echo $row_room['id']; ?>" class="btn btn-success btn-circle"><i class="fas fa-edit"></i></a></<a>
														<a onclick="return confirm('Adakah anda pasti ingin memadam bilik ini?')" href="room_list.php?delete=<?php echo $row_room['id']; ?>" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a>
													</center>
												</td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->