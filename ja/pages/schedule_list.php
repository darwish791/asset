<?php include "../includes/admin_header.php"; ?>

<?php
										

	if(isset($_GET['id'])){
					
	$room_id = $_GET['id'];
	$query_schedules = "SELECT * FROM schedules WHERE room_id = $room_id";
	$result_schedules = mysqli_query($connection, $query_schedules);

	}

	if(isset($_GET['delete'])){

		$schedules_id = $_GET['delete'];

		$query_schedules2 = "SELECT * FROM schedules WHERE id = $schedules_id";
		$result_schedules2 = mysqli_query($connection, $query_schedules2);
		$row_schedules2 = mysqli_fetch_assoc($result_schedules2);
		
		$sch_id = $row_schedules2['id'];
		$room_id2 = $row_schedules2['room_id'];
								
		$query_to_delete = "DELETE FROM schedules WHERE id = $sch_id";
		$result_to_delete = mysqli_query($connection, $query_to_delete);


		header("Location: schedule_list.php?id=$room_id2");
		
		// echo "<script>location.href='schedule_list.php?id=$room_id'</script>";
	}
?>



<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Senarai Jadual</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Subjek</th>
                      <th>Seksyen</th>
                      <th>Pensyarah</th>
                      <th>Program</th>
                      <th>Masa Mula</th>
                      <th>Masa Tamat</th>
                      <th>Action</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Bil</th>
                      <th>Subjek</th>
                      <th>Seksyen</th>
                      <th>Pensyarah</th>
                      <th>Program</th>
                      <th>Masa Mula</th>
                      <th>Masa Tamat</th>
											<th>Action</th>
											<th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_schedules = mysqli_fetch_assoc($result_schedules)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_schedules['subject'];  ?></td>
												<td><?php echo $row_schedules['section']; ?></td>
												<td><?php echo $row_schedules['lecturer']; ?></td>
												<td><?php echo $row_schedules['programme']; ?></td>
												<td><?php echo $row_schedules['start_time']; ?></td>
												<td><?php echo $row_schedules['end_time']; ?></td>
												<td><a href="room_schedule_update.php?id=<?php echo $row_schedules['id']; ?>" class="btn btn-primary">Kemaskini</a></td>
												<td><a onclick="return confirm('Adakah anda pasti ingin memadam subjek ini?')" href="schedule_list.php?delete=<?php echo $row_schedules['id']; ?>" class="btn btn-danger"><span ></span>Padam</a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->