<?php include "../includes/admin_header.php"; ?>
<?php $password_alert = false; ?>
<?php $password_success = false; 

if(isset($_POST["tkl"])) {

	$old_password = mysqli_real_escape_string($connection, $_POST["old_password"]);
	$new_password = mysqli_real_escape_string($connection, $_POST["new_password"]);
	$repeat_new_password = mysqli_real_escape_string($connection, $_POST["repeat_new_password"]);
	$db_password = $_SESSION['user_password'];
	$user_id = $_SESSION['user_id'];
	echo "<script>console.log('$old_password');</script>";
	echo "<script>console.log('$db_password');</script>";
	echo "<script>console.log('$new_password');</script>";
	echo "<script>console.log('$repeat_new_password');</script>";
	echo "<script>console.log('$user_id');</script>";

	if(password_verify($old_password,$db_password))	{
			echo "<script>console.log('password verify');</script>";

			if ($new_password === $repeat_new_password) {

				echo "<script>console.log('ok');</script>";
				$new_password = password_hash($new_password, PASSWORD_BCRYPT, array('cost' => 12));

				$query_update = "UPDATE users SET user_password='$new_password' WHERE user_id='$user_id'"; 
				$result_update = mysqli_query($connection, $query_update);
				
				$password_success = true;
				$message = "Tukar kata laluan berjaya";
		
			} else {
				$password_alert = true;
				$message = "Kata laluan tidak sepadan";
				echo "<script>console.log('Kata Laluan tidak sepadan');</script>";
			}
	}	else {
	$password_alert = true;
	$message = "Kata laluan lama salah";
	echo "<script>console.log('Kata Laluan lama salah');</script>";
	}

	
}

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

				<?php if($password_alert) : ?>
					<div class="alert alert-danger" role="alert">
						<?php echo $message; ?>
					</div>
				<?php endif; ?>

				<?php if($password_success) : ?>
					<div class="alert alert-success" role="alert">
						<?php echo $message; ?>
					</div>
				<?php endif; ?>

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Hi <?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name']; ?></h1>
        </div>
        <!-- /.container-fluid -->

				<!-- Basic Card Example -->
				<div class="card shadow mb-4">
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Maklumat Pengguna</h6>
					</div>
					<div class="card-body">
						<p>Nama: <?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name']; ?></p>
						<p>No Matrik: <?php echo $_SESSION['no_matric']; ?></p>
						<p>Email: <?php echo $_SESSION['user_email']; ?></p>
					</div>
				</div>

				<!-- Collapsable Card Example -->
				<div class="card shadow mb-4">
					<!-- Card Header - Accordion -->
					<a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
						<h6 class="m-0 font-weight-bold text-primary">Tukar Kata laluan</h6>
					</a>
					<!-- Card Content - Collapse -->
					<div class="collapse" id="collapseCardExample">
						<div class="card-body">
							<form action="" method="post">
									<label for="">Masukkan kata laluan lama:</label>
									<input type="password" class="form-control" name="old_password" required>

									<label for="">Masukkan kata laluan baharu:</label>
									<input type="password" class="form-control" name="new_password" required>

									<label for="">Masukkan kata laluan baharu:</label>
									<input type="password" class="form-control" name="repeat_new_password" required>

									<div class="modal-footer">
										<button class="btn btn-secondary" type="button" data-toggle="collapse" aria-controls="collapseCardExample" href="#collapseCardExample">Batal</button>
										<input name="tkl" type="submit" class="btn btn-primary" value="Hantar"></input>
									</div>
							</form>
						</div>
					</div>
				</div>

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
 

  
