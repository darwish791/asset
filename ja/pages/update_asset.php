<?php include "../includes/admin_header.php"; ?>

<?php 

	$asset_id = $_GET['id'];

	$query_display = "SELECT * FROM assets WHERE id = $asset_id";
	$result_display = mysqli_query($connection, $query_display);
	$row_display = mysqli_fetch_assoc($result_display);
 ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <center><h1 class="h3 mb-4 text-gray-800">Kemaskini Aset</h1></center>

					<div class="container">

						<!-- Outer Row -->
						<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-12">
										<div class="p-5">
											<form class="user" method="post" action="">
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Aset</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="Contoh: Nikon D5600 DSLR" name="asset_name" value="<?php echo $row_display['asset_name']; ?>" required>
												</div>
												
												<div class="form-group">
													<label for="name" style="margin-left:18px">Pembekal</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="Contoh: Basenet Technology Sdn Bhd" name="supplier_name" value="<?php echo $row_display['supplier_name']; ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tarikh Akhir Jaminan</label>
													<input type="date" class="form-control form-control-user" id="" name="tarikh_akhir_jaminan" value="<?php echo $row_display['tarikh_akhir_jaminan']; ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nombor Tender</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="Contoh: UTHM(P)04/008/2013" name="tender_no" value="<?php echo $row_display['tender_no']; ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Talian Helpdesk</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="Contoh: 07-4537292@7295" name="talian_helpdesk" value="<?php echo $row_display['talian_helpdesk']; ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nombor Siri Aset</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="Contoh: 562RQ02" name="no_siri" value="<?php echo $row_display['no_siri']; ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Butiran</label>
													<textarea rows="7" cols="50"  class="form-control" placeholder="Butiran aset..." name="asset_description"></textarea>
												</div>
												<div class="form-group">
													<label for="ja" style="margin-left:18px">Penolong Jurutera Bertugas</label>
													<select id="ja" class="form-control" name="asset_ja_id" required>
														<option value="">-- Pilih --</option>
														<?php 
														$query_ja = "SELECT * FROM users WHERE user_role = \"Penolong Jurutera\"";
														$result_ja = mysqli_query($connection, $query_ja);
														while($row_ja = mysqli_fetch_array($result_ja)): ?>
															<option value="<?php echo $row_ja['user_id']; ?>" name="ja"><?php echo $row_ja['first_name'] . " " . $row_ja['last_name']; ?></option>
														<?php endwhile; ?>						
													</select>
												</div>
												<input type="submit" class="btn btn-primary btn-user btn-block" name="submit" value="Kemaskini">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
			
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

<?php

if (isset($_POST['submit'])) {

	$asset_name = mysqli_real_escape_string($connection, $_POST['asset_name']);
	$supplier_name = mysqli_real_escape_string($connection, $_POST['supplier_name']);
	$tarikh_akhir_jaminan = mysqli_real_escape_string($connection, $_POST['tarikh_akhir_jaminan']);
	$tender_no = mysqli_real_escape_string($connection, $_POST['tender_no']);
	$talian_helpdesk = mysqli_real_escape_string($connection, $_POST['talian_helpdesk']);
	$no_siri = mysqli_real_escape_string($connection, $_POST['no_siri']);
	$asset_description = mysqli_real_escape_string($connection, $_POST['asset_description']);
	$asset_ja_id = mysqli_real_escape_string($connection, $_POST['asset_ja_id']);

	$query_update = "UPDATE assets SET asset_name='$asset_name', supplier_name='$supplier_name', tarikh_akhir_jaminan='$tarikh_akhir_jaminan', tender_no='$tender_no', talian_helpdesk='$talian_helpdesk', no_siri='$no_siri', asset_description='$asset_description', asset_ja_id='$asset_ja_id' WHERE id='$asset_id'"; 
	$result = mysqli_query($connection, $query_update);
	header("Location: asset_list.php");
}

?>