<?php include "../includes/admin_header.php"; ?>

<?php 

	if (isset($_GET['id'])) {
		
		$asset_id = $_GET['id'];
	}
										
	$query_asset = "SELECT * FROM assets WHERE id= $asset_id";
	$result_asset = mysqli_query($connection, $query_asset);
	$row_asset = mysqli_fetch_assoc($result_asset);
?>



<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $row_asset['asset_name'] ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <tbody>
											
											<tr>
												<th width="35%">Pembekal</th>
												<td width="65%"><?php echo $row_asset['supplier_name'] ?></td>
											</tr>
											
											<tr >
												<th>Tarikh Akhir Jaminan</th>
												<td><?php echo $row_asset['tarikh_akhir_jaminan'] ?></td>
											</tr>

											<tr >
												<th>Nombor Tender</th>
												<td><?php echo $row_asset['tender_no'] ?></td>
											</tr>

											<tr>
												<th>Talian Helpdesk</th>
												<td><?php echo $row_asset['talian_helpdesk'] ?></td>
											</tr>
											
											<tr >
												<th>Nombor Siri Aset</th>
												<td><?php echo $row_asset['no_siri'] ?></td>
											</tr>

											<tr >
												<th>Butiran</th>
												<td><?php echo $row_asset['asset_description'] ?></td>
											</tr>

											<tr >
												<?php
													$ja_id = $row_asset['asset_ja_id'];

													$query_ja = "SELECT * FROM users WHERE user_id= $ja_id";
													$result_ja = mysqli_query($connection, $query_ja);
													$row_ja = mysqli_fetch_assoc($result_ja);
												?>
												<th>Penolong Jurutera Bertugas</th>
												<td><?php echo $row_ja['first_name'] . " " . $row_ja['last_name']?></td>
											</tr>

											<tr >
												<th>Status</th>
												<td><?php echo $row_asset['status'] ?></td>
											</tr>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->