<?php include "../includes/admin_header.php"; ?>
<?php $approval_success = false; ?>
<?php $approval_alert = false; ?>

<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

?>

<?php 
	
	if (isset($_GET['unapprove'])) {
		
		$reservation_id_to_unapprove = $_GET['unapprove'];

		//query to display info in the email
		$query_to_display = "SELECT * FROM schedules WHERE id = $reservation_id_to_unapprove";
		$result_to_display = mysqli_query($connection, $query_to_display);
		$row_to_display = mysqli_fetch_array($result_to_display);

		//query to display room name based on the room id
		$room_id_to_display = $row_to_display['room_id'];
		$query_room_to_display = "SELECT * FROM rooms WHERE id = $room_id_to_display";
		$result_room_to_display = mysqli_query($connection, $query_room_to_display);
		$row_room_to_display = mysqli_fetch_array($result_room_to_display);

		//assign receipent to user's email
		$user_no_matric = $row_to_display['no_matric'];
		$query_user = "SELECT * FROM users WHERE no_matric = '$user_no_matric'";
		$result_user = mysqli_query($connection, $query_user);
		$row_user = mysqli_fetch_array($result_user);
		$mailto = $row_user['user_email'];

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'datacenteruthm@gmail.com';                     // SMTP username
			$mail->Password   = 'kendecostonaten';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
			$mail->SMTPSecure = 'ssl';        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
			$mail->Port       = 465;                                    // TCP port to connect to
			
			$mail->SMTOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			$mail->setFrom('datacenteruthm@gmail.com', 'Pusat Data UTHM');
			$mail->addAddress($mailto);     // Add a recipient
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');

			// // Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Tempahan Anda TIDAK Diluluskan';
			$mail->Body    = 'Ruang: ' .$row_room_to_display['name']. '<br>Waktu: Sepanjang Semester <br>Masa: ' .$row_to_display['start_time']. ' hingga ' .$row_to_display['end_time']. '<br>Penempah: ' .$row_to_display['reserver']. '<br><br>Harap maaf. Permohonan anda tidak diluluskan. Hal ini kerana ruang tersebut sudah ditempah atau sedang menjalani kerja-kerja penyelenggaraan';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			$approval_success = true;
			$message = "Pengesahan tempahan Berjaya";
			//query to update status
			$query_to_unapprove = "UPDATE schedules SET status = \"Unapproved\" WHERE id = $reservation_id_to_unapprove";
			$result_to_unapprove = mysqli_query($connection, $query_to_unapprove);
		} catch (Exception $e) {
			$approval_alert = true;
			$message = "Pengesahan tempahan tidak berjaya. Sila cuba lagi. Terdapat ralat: {$mail->ErrorInfo}";
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		// header("Location: reservation_approval.php");
	}

	if (isset($_GET['approve'])) {
		
		$reservation_id_to_approve = $_GET['approve'];

		//query to display info in the email
		$query_to_display = "SELECT * FROM schedules WHERE id = $reservation_id_to_approve";
		$result_to_display = mysqli_query($connection, $query_to_display);
		$row_to_display = mysqli_fetch_array($result_to_display);

		//query to display room name based on the room id
		$room_id_to_display = $row_to_display['room_id'];
		$query_room_to_display = "SELECT * FROM rooms WHERE id = $room_id_to_display";
		$result_room_to_display = mysqli_query($connection, $query_room_to_display);
		$row_room_to_display = mysqli_fetch_array($result_room_to_display);

		//assign receipent to user's email
		$user_no_matric = $row_to_display['no_matric'];
		$query_user = "SELECT * FROM users WHERE no_matric = '$user_no_matric'";
		$result_user = mysqli_query($connection, $query_user);
		$row_user = mysqli_fetch_array($result_user);
		$mailto = $row_user['user_email'];

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'datacenteruthm@gmail.com';                     // SMTP username
			$mail->Password   = 'kendecostonaten';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
			$mail->SMTPSecure = 'ssl';        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
			$mail->Port       = 465;                                    // TCP port to connect to
			
			$mail->SMTOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			$mail->setFrom('datacenteruthm@gmail.com', 'Pusat Data UTHM');
			$mail->addAddress($mailto);     // Add a recipient
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');

			// // Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Tempahan Anda Telah Diluluskan';
			$mail->Body    = 'Ruang: ' .$row_room_to_display['name']. '<br>Waktu: Sepanjang Semester <br>Masa: ' .$row_to_display['start_time']. ' hingga ' .$row_to_display['end_time']. '<br>Penempah: ' .$row_to_display['reserver']. '<br><br>Permohonan anda telah diluluskan. Sila datang pada tarikh dan masa yang ditetapkan. Terima kasih';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			$approval_success = true;
			$message = "Pengesahan tempahan Berjaya";
			//query to update status
			$query_to_approve = "UPDATE schedules SET status = \"Approved\" WHERE id = $reservation_id_to_approve";
			$result_to_approve = mysqli_query($connection, $query_to_approve);
		} catch (Exception $e) {
			$approval_alert = true;
			$message = "Pengesahan tempahan tidak berjaya. Sila cuba lagi. Terdapat ralat: {$mail->ErrorInfo}";
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		// header("Location: reservation_approval.php");
	}
	
?>

<?php

$query_reservation = "SELECT * FROM schedules WHERE status = 'Pending'";
$result_reservation = mysqli_query($connection, $query_reservation);

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

				<?php if($approval_success) : ?>
					<div class="alert alert-success" role="alert">
						<?php echo $message; ?>
					</div>
				<?php endif; ?>

				<?php if($approval_alert) : ?>
					<div class="alert alert-danger" role="alert">
						<?php echo $message; ?>
					</div>
				<?php endif; ?>

				<!-- Content Row -->
				<div class="row">

				<div class="col-xl-3 col-md-6 mb-4">
					<a href="reservation_approval.php">
						<div class="card border-left-success shadow h-100 py-2">
							<div class="card-body">
								<div class="row no-gutters align-items-center">
									<div class="col mr-2">
										<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Tempahan Mingguan</div>
										<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
									</div>
									<div class="col-auto">
										<i class="fas fa-calendar-check fa-2x text-gray-300"></i>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
				</div>
				<br>

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Senarai Tempahan Baru Sepanjang Semester</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
											<th>Bil</th>
                      <th>Nama Pelajar</th>
                      <th>No Matrik</th>
                      <th>Ruang</th>
                      <th>Subject</th>
                      <th>Pensyarah</th>
                      <th>Program</th>
                      <th>Masa Mula</th>
                      <th>Masa Akhir</th>
                      <th>Sahkan</th>
                      <th>Batalkan</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
											<th>Bil</th>
                      <th>Nama Pelajar</th>
                      <th>No Matrik</th>
                      <th>Ruang</th>
                      <th>Subject</th>
                      <th>Pensyarah</th>
                      <th>Program</th>
                      <th>Masa Mula</th>
                      <th>Masa Akhir</th>
                      <th>Sahkan</th>
                      <th>Batalkan</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_reservation = mysqli_fetch_assoc($result_reservation)) : ?>

											<tr>
											<?php

													$room_id = $row_reservation['room_id'];
													$query_room = "SELECT * FROM rooms WHERE id = $room_id";
													$result_room = mysqli_query($connection, $query_room);
													$row_room = mysqli_fetch_array($result_room);
											?>

												<td><?php echo $i; ?></td>
												<td><?php echo $row_reservation['reserver']; ?></td>
												<td><?php echo $row_reservation['no_matric']; ?></td>
												<td><?php echo $row_room['name']; ?></td>
												<td><?php echo $row_reservation['subject']; ?></td>
												<td><?php echo $row_reservation['lecturer']; ?></td>
												<td><?php echo $row_reservation['programme']; ?></td>
												<td><?php echo $row_reservation['start_time']; ?></td>
												<td><?php echo $row_reservation['end_time']; ?></td>
												<td><a onclick="return confirm('Anda pasti ingin mengesahkan tempahan ini?')" href="reservation_approval_sem.php?approve=<?php echo $row_reservation['id']; ?>" class="btn btn-success"><span ></span> Sah </a></td>
												<td><a onclick="return confirm('Anda pasti ingin membatalkan tempahan ini?')" href="reservation_approval_sem.php?unapprove=<?php echo $row_reservation['id']; ?>" class="btn btn-danger"><span ></span> Batal </a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->