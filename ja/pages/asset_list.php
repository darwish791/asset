<?php include "../includes/admin_header.php"; ?>

<?php 
										
	$query_asset = "SELECT * FROM assets WHERE status = 'Sedia'";
	$result_asset = mysqli_query($connection, $query_asset);

	if (isset($_GET['availability'])) {
		
		$availability = $_GET['availability'];
		if ($availability == 1) {
			$availability = 'Sedia';
		} else {
			$availability = 'Dalam Tempahan';
		}

		$query_asset = "SELECT * FROM assets WHERE status = '$availability'";
		$result_asset = mysqli_query($connection, $query_asset);
	}

	if(isset($_GET['delete'])){

		$asset_id = $_GET['delete'];
		$query_to_delete = "DELETE FROM assets WHERE id = $asset_id";
		$result_to_delete = mysqli_query($connection, $query_to_delete);
		header("Location: asset_list.php");
	}
?>



<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Senarai Asset</h1>

					<!-- Content Row -->
          <div class="row">

						<!-- Sedia -->
							<div class="col-xl-3 col-md-6 mb-4">
								<a href="asset_list.php?availability=1">
									<div class="card border-left-success shadow h-100 py-2">
										<div class="card-body">
											<div class="row no-gutters align-items-center">
												<div class="col mr-2">
													<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Sedia</div>
												</div>
												<div class="col-auto">
													<i class="fas fa-calendar-check fa-2x text-gray-300"></i>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>

							<!-- Dalam Tempahan -->
							<div class="col-xl-3 col-md-6 mb-4">
								<a href="asset_list.php?availability=2">
									<div class="card border-left-danger shadow h-100 py-2">
										<div class="card-body">
											<div class="row no-gutters align-items-center">
												<div class="col mr-2">
													<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Dalam Tempahan</div>
												</div>
												<div class="col-auto">
													<i class="fas fa-hourglass-end fa-2x text-gray-300"></i>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>

					</div>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>Nombor Siri</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>Nombor Siri</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_asset = mysqli_fetch_assoc($result_asset)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_asset['asset_name'];  ?></td>
												<td><?php echo $row_asset['no_siri']; ?></td>
												<td><?php echo $row_asset['status']; ?></td>
												<td>
													<center>
														<a href="view_asset.php?id=<?php echo $row_asset['id']; ?>" class="btn btn-primary btn-circle"><i class="fas fa-eye"></i></a>
														<a href="update_asset.php?id=<?php echo $row_asset['id']; ?>" class="btn btn-success btn-circle"><i class="fas fa-edit"></i></a></<a>
														<a onclick="return confirm('Adakah anda pasti ingin memadam asset ini?')" href="asset_list.php?delete=<?php echo $row_asset['id']; ?>" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a>
													</center>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->