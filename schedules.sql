-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2019 at 05:24 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `subject` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `lecturer` varchar(255) NOT NULL,
  `programme` varchar(255) NOT NULL,
  `day_schedule_id` int(11) NOT NULL,
  `room_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `start_time`, `end_time`, `subject`, `section`, `lecturer`, `programme`, `day_schedule_id`, `room_id`) VALUES
(12, '03:15:00', '05:45:00', 'BIC24044', '2', 'Rahayu', '1BIM', 1, 1),
(13, '03:45:00', '02:50:00', 'BIC24044', '2', 'Rahayu', '1BIM', 2, 1),
(14, '08:00:00', '10:00:00', 'BIC24044', '2', 'Rahayu', '1BIM', 2, 1),
(15, '08:00:00', '10:00:00', 'BIC24044', '2', 'Rahayu', '1BIM', 1, 1),
(16, '08:00:00', '10:00:00', 'bic', '2', 'Rahayu', '1bim', 2, 1),
(17, '08:00:00', '10:00:00', 'BIC', '2', 'RAHAYU', '1BIM', 2, 1),
(18, '04:20:00', '08:20:00', 'BIC', '4', 'AAAA', 'AAAA', 4, 0),
(19, '02:10:00', '14:10:00', 'BIC', '4', 'DVSWD', '1BIM', 3, 2),
(20, '08:00:00', '10:00:00', 'BIC', '2', 'EWFVWF', 'DCWD', 3, 2),
(21, '08:00:00', '10:00:00', 'BIC', '3', 'EWFVWF', 'DCWD', 5, 0),
(22, '08:00:00', '10:00:00', 'BIC', '4', 'DVSWD', '1BIM', 3, 3),
(23, '08:00:00', '10:00:00', 'BIC', '2', 'RAHAYU', '1BIM', 4, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
