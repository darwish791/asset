<?php include "../includes/admin_header.php"; ?>

<?php 
										
	$query_approved_user = "SELECT * FROM users WHERE user_role = \"Pengguna\"";
	$result_approved_user = mysqli_query($connection, $query_approved_user);
	

	$query_unapproved_user = "SELECT * FROM users WHERE user_role = \"Pengguna Belum Disahkan\"";
	$result_unapproved_user = mysqli_query($connection, $query_unapproved_user);
?>



<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Senarai Pengguna</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>No Matrik</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>No Matrik</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_approved_user = mysqli_fetch_assoc($result_approved_user)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_approved_user['first_name'] . " " . $row_approved_user['last_name'];  ?></td>
												<td><?php echo $row_approved_user['no_matric']; ?></td>
												<td><?php echo $row_approved_user['user_email']; ?></td>
												<td><?php echo $row_approved_user['user_role']; ?></td>
												<td><a href="user.php?unapprove=<?php echo $row_approved_user['user_id']; ?>" class="btn btn-danger">Lucut</a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>

										<?php while($row_unapproved_user = mysqli_fetch_assoc($result_unapproved_user)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_unapproved_user['first_name'] . " " . $row_unapproved_user['last_name'];  ?></td>
												<td><?php echo $row_unapproved_user['no_matric']; ?></td>
												<td><?php echo $row_unapproved_user['user_email']; ?></td>
												<td><?php echo $row_unapproved_user['user_role']; ?></td>
												<td><a href="user.php?approve=<?php echo $row_unapproved_user['user_id']; ?>" class="btn btn-success">Sahkan</a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

<?php 

	if (isset($_GET['unapprove'])) {
		
		$user_id_to_unapprove = $_GET['unapprove'];

		$query_to_unapprove = "UPDATE users SET user_role = \"Pengguna Belum Disahkan\" WHERE user_id = $user_id_to_unapprove";
		$result_to_unapprove = mysqli_query($connection, $query_to_unapprove);
		header("Location: user.php");
	}

	if (isset($_GET['approve'])) {
		
		$user_id_to_approve = $_GET['approve'];

		$query_to_approve = "UPDATE users SET user_role = \"Pengguna\" WHERE user_id = $user_id_to_approve";
		$result_to_approve = mysqli_query($connection, $query_to_approve);
		header("Location: user.php");
	}



 ?>
