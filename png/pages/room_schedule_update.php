<?php include "../includes/admin_header.php"; ?>

<?php

	$schedule_id = $_GET['id'];

	$query_display = "SELECT * FROM schedules WHERE id = $schedule_id";
	$result_display = mysqli_query($connection, $query_display);
	$row_display = mysqli_fetch_assoc($result_display);
	$room_id = $row_display['room_id'];


	if (isset($_POST['submit'])) {
		
		$subject = strtoupper(mysqli_real_escape_string($connection, $_POST['subject']));
		$section = mysqli_real_escape_string($connection, $_POST['section']);
		$lecturer = strtoupper(mysqli_real_escape_string($connection, $_POST['lecturer']));
		$programme = strtoupper(mysqli_real_escape_string($connection, $_POST['programme']));
		$day_schedule_id = mysqli_real_escape_string($connection, $_POST['day_schedule_id']);
		$start_time = mysqli_real_escape_string($connection, $_POST['start_time']);
		$end_time = mysqli_real_escape_string($connection, $_POST['end_time']);

		$subject = strtoupper($subject);
		$lecturer = strtoupper($lecturer);
		$programme = strtoupper($programme);

		$query_update = "UPDATE schedules SET start_time='$start_time', end_time='$end_time', subject='$subject', section='$section', lecturer='$lecturer', programme='$programme', day_schedule_id='$day_schedule_id' WHERE id='$schedule_id'"; 
		$result_update = mysqli_query($connection, $query_update);
		header("Location: schedule_list.php?id=$room_id");
	}
 ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
					<div class="container">

						<!-- Outer Row -->
						<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-12">
										<div class="p-5">
											<center><h1 class="h3 mb-4 text-gray-800">Kemaskini Jadual</h1></center>
											<form class="user" method="post" action="">
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Subjek</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: BIC10204" name="subject" value="<?php echo $row_display['subject'] ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Seksyen</label>
													<input type="number" class="form-control form-control-user" id="" placeholder="eg: 2" name="section" value="<?php echo $row_display['section'] ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Pensyarah</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: Rahayu" name="lecturer" value="<?php echo $row_display['lecturer'] ?>" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Program</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: 1BIM" name="programme" value="<?php echo $row_display['programme'] ?>" required>
												</div>
												<div class="form-group">
													<label for="day" style="margin-left:18px">Hari</label>
													<select class="form-control" id="day" name="day_schedule_id" required>
														<option value="1" <?php echo ($row_display['day_schedule_id']=='1') ?'selected':'' ?> required>Ahad</option>
														<option value="2" <?php echo ($row_display['day_schedule_id']=='2') ?'selected':'' ?> required >Isnin</option>
														<option value="3" <?php echo ($row_display['day_schedule_id']=='3') ?'selected':'' ?> required>Selasa</option>
														<option value="4" <?php echo ($row_display['day_schedule_id']=='4') ?'selected':'' ?> required>Rabu</option>
														<option value="5" <?php echo ($row_display['day_schedule_id']=='5') ?'selected':'' ?> required>Khamis</option>
													</select>
												</div>

												<div class="form-group">
												<label for="start-time" style="margin-left:18px">Masa mula (Sistem 24 - jam)</label>
													<div class="col-md-4 col-md-offset-4"></div>
														<div class="input-group clockpicker" data-autoclose="true">
															<input name="start_time" type="text" class="form-control" value="<?php echo $row_display['start_time'] ?>" required>
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-time"></span>
															</span>
														</div>
												</div>

												<div class="form-group">
												<label for="start-time" style="margin-left:18px">Masa tamat (Sistem 24 - jam)</label>
													<div class="col-md-4 col-md-offset-4"></div>
														<div class="input-group clockpicker" data-autoclose="true">
															<input name="end_time" type="text" class="form-control" value="<?php echo $row_display['end_time'] ?>" required>
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-time"></span>
															</span>
														</div>
												</div>

												<input type="submit" class="btn btn-primary btn-user btn-block" name="submit" value="Simpan">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
			
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

<!-- Clockpicker -->
<script type="text/javascript">
	$('.clockpicker').clockpicker();
</script>
<!-- End Clockpicker -->