<?php include "../includes/admin_header.php"; ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <center><h1 class="h3 mb-4 text-gray-800">Tambah Bilik Baharu</h1></center>

					<div class="container">

						<!-- Outer Row -->
						<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-12">
										<div class="p-5">
											<form class="user" method="post" action="">
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Bilik</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: Makmal Cisco" name="room_name" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Kapasiti</label>
													<input type="number" class="form-control form-control-user" id="" placeholder="eg: 100" name="room_capacity" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Butiran</label>
													<textarea rows="7" cols="50"  class="form-control" placeholder="Butiran makmal..." name="room_description"></textarea>
												</div>
												<div class="form-group">
													<label for="aras" style="margin-left:18px">Aras</label>
													<select class="form-control" id="aras" name="room_level" required>
														<option value="">-- Pilih --</option>
														<option value="Aras Bawah">Aras Bawah</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
													</select>
												</div>
												<div class="form-group">
													<label for="ja" style="margin-left:18px">Penolong Jurutera Bertugas</label>
													<select id="ja" class="form-control" name="room_ja_id" required>
														<option value="">-- Pilih --</option>
														<?php 
														$query_ja = "SELECT * FROM users WHERE user_role = \"Penolong Jurutera\"";
														$result_ja = mysqli_query($connection, $query_ja);
														while($row_ja = mysqli_fetch_array($result_ja)): ?>
															<option value="<?php echo $row_ja['user_id']; ?>" name="ja"><?php echo $row_ja['first_name'] . " " . $row_ja['last_name']; ?></option>
														<?php endwhile; ?>						
													</select>
												</div>
												<input type="submit" class="btn btn-primary btn-user btn-block" name="submit" value="Simpan">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
			
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

<?php

if (isset($_POST['submit'])) {

	$room_name = mysqli_real_escape_string($connection, $_POST['room_name']);
	$room_capacity = mysqli_real_escape_string($connection, $_POST['room_capacity']);
	$room_description = mysqli_real_escape_string($connection, $_POST['room_description']);
	$room_level = mysqli_real_escape_string($connection, $_POST['room_level']);
	$room_ja_id = mysqli_real_escape_string($connection, $_POST['room_ja_id']);

	$query = "INSERT INTO rooms (name, capacity, status, room_description, room_level, room_ja_id) ";
	$query .= "VALUES ('$room_name', $room_capacity, 'Ready', '$room_description', '$room_level', $room_ja_id)";
	$result = mysqli_query($connection, $query);
	header("Location: room_list.php");
}

?>