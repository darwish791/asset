<?php include "../includes/admin_header.php"; ?>

<?php 
										
	$query_approved_ja = "SELECT * FROM users WHERE user_role = \"Penolong Jurutera\"";
	$result_approved_ja = mysqli_query($connection, $query_approved_ja);
	

	$query_unapproved_ja = "SELECT * FROM users WHERE user_role = \"Penolong Jurutera Belum Disahkan\"";
	$result_unapproved_ja = mysqli_query($connection, $query_unapproved_ja);
?>



<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Senarai Penolong Jurutera</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>No Staff</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>No Staff</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_approved_ja = mysqli_fetch_assoc($result_approved_ja)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_approved_ja['first_name'] . " " . $row_approved_ja['last_name'];  ?></td>
												<td><?php echo $row_approved_ja['no_matric']; ?></td>
												<td><?php echo $row_approved_ja['user_email']; ?></td>
												<td><?php echo $row_approved_ja['user_role']; ?></td>
												<td><a href="ja.php?unapprove=<?php echo $row_approved_ja['user_id']; ?>" class="btn btn-danger">Lucut</a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>

										<?php while($row_unapproved_ja = mysqli_fetch_assoc($result_unapproved_ja)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_unapproved_ja['first_name'] . " " . $row_unapproved_ja['last_name'];  ?></td>
												<td><?php echo $row_unapproved_ja['no_matric']; ?></td>
												<td><?php echo $row_unapproved_ja['user_email']; ?></td>
												<td><?php echo $row_unapproved_ja['user_role']; ?></td>
												<td><a href="ja.php?approve=<?php echo $row_unapproved_ja['user_id']; ?>" class="btn btn-success">Sahkan</a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

<?php 

	if (isset($_GET['unapprove'])) {
		
		$user_id_to_unapprove = $_GET['unapprove'];

		$query_to_unapprove = "UPDATE users SET user_role = \"Penolong Jurutera Belum Disahkan\" WHERE user_id = $user_id_to_unapprove";
		$result_to_unapprove = mysqli_query($connection, $query_to_unapprove);
		header("Location: ja.php");
	}

	if (isset($_GET['approve'])) {
		
		$user_id_to_approve = $_GET['approve'];

		$query_to_approve = "UPDATE users SET user_role = \"Penolong Jurutera\" WHERE user_id = $user_id_to_approve";
		$result_to_approve = mysqli_query($connection, $query_to_approve);
		header("Location: ja.php");
	}



 ?>
