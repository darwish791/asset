<?php include "../includes/admin_header.php"; ?>
<?php $approval_success = false; ?>
<?php $approval_alert = false; ?>
<?php $default = true; ?>

<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

?>

<?php 

	if(isset($_POST['submit'])){

		$reservation_id_to_approve = $_POST['reservation_id'];
		$ja_id_assign = $_POST['room_ja_id'];
		
		//query to display info in the email
		$query_to_display = "SELECT * FROM tempahan WHERE id = $reservation_id_to_approve";
		$result_to_display = mysqli_query($connection, $query_to_display);
		$row_to_display = mysqli_fetch_array($result_to_display);

		//query to display room name based on the room id
		$room_id_to_display = $row_to_display['room_id'];
		$query_room_to_display = "SELECT * FROM rooms WHERE id = $room_id_to_display";
		$result_room_to_display = mysqli_query($connection, $query_room_to_display);
		$row_room_to_display = mysqli_fetch_array($result_room_to_display);

		//query to display ja_assign_id
		$query_ja_assign = "SELECT * FROM users WHERE user_id = $ja_id_assign";
		$result_ja_assign = mysqli_query($connection, $query_ja_assign);
		$row_ja_assign = mysqli_fetch_array($result_ja_assign);

		//assign receipent to user's email
		$mailto = $row_to_display['user_email'];
		$mailto2 = $row_ja_assign['user_email'];

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'datacenteruthm@gmail.com';                     // SMTP username
			$mail->Password   = 'kendecostonaten';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
			$mail->SMTPSecure = 'ssl';        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
			$mail->Port       = 465;                                    // TCP port to connect to
			
			$mail->SMTOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			$mail->setFrom('datacenteruthm@gmail.com', 'Pusat Data UTHM');
			$mail->addAddress($mailto);     // Add a recipient
			$mail->addAddress($mailto2);     // Add a recipient
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');

			// // Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Tempahan Telah Diluluskan';
			$mail->Body    = 'Ruang: ' .$row_room_to_display['name']. '<br>Tarikh: ' .$row_to_display['start_date']. ' hingga ' .$row_to_display['end_date']. '<br>Masa: ' .$row_to_display['start_time']. ' hingga ' .$row_to_display['end_time']. '<br>Penempah: ' .$row_to_display['name']. '<br>Penolong Jurutera Bertugas: ' .$row_ja_assign['first_name']. ' ' .$row_ja_assign['last_name']. '<br><br>Permohonan telah diluluskan. Sila datang pada tarikh dan masa yang ditetapkan. <br><br>Kepada penolong jurutera yang telah ditugaskan, sila hadir pada masa dan tarikh yang ditetapkan untuk melancarkan urusan penempahan. <br><br>Terima kasih';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			$approval_success = true;
			$message = "Pengesahan tempahan Berjaya";
			//query to update status
			$query_to_approve = "UPDATE tempahan SET status = \"Approved\" WHERE id = $reservation_id_to_approve";
			$result_to_approve = mysqli_query($connection, $query_to_approve);
		} catch (Exception $e) {
			$approval_alert = true;
			$message = "Pengesahan tempahan tidak berjaya. Sila cuba lagi. Terdapat ralat: {$mail->ErrorInfo}";
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		// header("Location: reservation_approval.php");
	}
	
	if (isset($_GET['unapprove'])) {
		
		$reservation_id_to_unapprove = $_GET['unapprove'];

		//query to display info in the email
		$query_to_display = "SELECT * FROM tempahan WHERE id = $reservation_id_to_unapprove";
		$result_to_display = mysqli_query($connection, $query_to_display);
		$row_to_display = mysqli_fetch_array($result_to_display);

		//query to display room name based on the room id
		$room_id_to_display = $row_to_display['room_id'];
		$query_room_to_display = "SELECT * FROM rooms WHERE id = $room_id_to_display";
		$result_room_to_display = mysqli_query($connection, $query_room_to_display);
		$row_room_to_display = mysqli_fetch_array($result_room_to_display);

		//assign receipent to user's email
		$mailto = $row_to_display['user_email'];

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'datacenteruthm@gmail.com';                     // SMTP username
			$mail->Password   = 'kendecostonaten';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
			$mail->SMTPSecure = 'ssl';        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
			$mail->Port       = 465;                                    // TCP port to connect to
			
			$mail->SMTOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			$mail->setFrom('datacenteruthm@gmail.com', 'Pusat Data UTHM');
			$mail->addAddress($mailto);     // Add a recipient
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');

			// // Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Tempahan Anda TIDAK Diluluskan';
			$mail->Body    = 'Ruang: ' .$row_room_to_display['name']. '<br>Tarikh: ' .$row_to_display['start_date']. ' hingga ' .$row_to_display['end_date']. '<br>Masa: ' .$row_to_display['start_time']. ' hingga ' .$row_to_display['end_time']. '<br>Penempah: ' .$row_to_display['name']. '<br><br>Harap maaf. Permohonan anda tidak diluluskan. Hal ini kerana ruang tersebut sudah ditempah atau sedang menjalani kerja-kerja penyelenggaraan';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			$approval_success = true;
			$message = "Pengesahan tempahan Berjaya";
			//query to update status
			$query_to_unapprove = "UPDATE tempahan SET status = \"Unapproved\" WHERE id = $reservation_id_to_unapprove";
			$result_to_unapprove = mysqli_query($connection, $query_to_unapprove);
		} catch (Exception $e) {
			$approval_alert = true;
			$message = "Pengesahan tempahan tidak berjaya. Sila cuba lagi. Terdapat ralat: {$mail->ErrorInfo}";
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		// header("Location: reservation_approval.php");
	}

	if (isset($_GET['approve'])) {
		
		$reservation_id_to_approve = $_GET['approve'];

		//query to display info in the email
		$query_to_display = "SELECT * FROM tempahan WHERE id = $reservation_id_to_approve";
		$result_to_display = mysqli_query($connection, $query_to_display);
		$row_to_display = mysqli_fetch_array($result_to_display);

		//query to display room name based on the room id
		$room_id_to_display = $row_to_display['room_id'];
		$query_room_to_display = "SELECT * FROM rooms WHERE id = $room_id_to_display";
		$result_room_to_display = mysqli_query($connection, $query_room_to_display);
		$row_room_to_display = mysqli_fetch_array($result_room_to_display);

		//assign receipent to user's email
		$mailto = $row_to_display['user_email'];

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'datacenteruthm@gmail.com';                     // SMTP username
			$mail->Password   = 'kendecostonaten';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
			$mail->SMTPSecure = 'ssl';        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
			$mail->Port       = 465;                                    // TCP port to connect to
			
			$mail->SMTOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			$mail->setFrom('datacenteruthm@gmail.com', 'Pusat Data UTHM');
			$mail->addAddress($mailto);     // Add a recipient
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');

			// // Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Tempahan Anda Telah Diluluskan';
			$mail->Body    = 'Ruang: ' .$row_room_to_display['name']. '<br>Tarikh: ' .$row_to_display['start_date']. ' hingga ' .$row_to_display['end_date']. '<br>Masa: ' .$row_to_display['start_time']. ' hingga ' .$row_to_display['end_time']. '<br>Penempah: ' .$row_to_display['name']. '<br><br>Permohonan anda telah diluluskan. Sila datang pada tarikh dan masa yang ditetapkan. Terima kasih';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			$approval_success = true;
			$message = "Pengesahan tempahan Berjaya";
			//query to update status
			$query_to_approve = "UPDATE tempahan SET status = \"Approved\" WHERE id = $reservation_id_to_approve";
			$result_to_approve = mysqli_query($connection, $query_to_approve);
		} catch (Exception $e) {
			$approval_alert = true;
			$message = "Pengesahan tempahan tidak berjaya. Sila cuba lagi. Terdapat ralat: {$mail->ErrorInfo}";
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		// header("Location: reservation_approval.php");
	}
		
?>

<?php 

$query_reservation_default = "SELECT * FROM tempahan WHERE status = 'Pending' AND WEEKDAY(start_date) IN (0, 1, 2, 3, 6)";
$result_reservation_default = mysqli_query($connection, $query_reservation_default);

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

					<?php if($approval_success) : ?>
						<div class="alert alert-success" role="alert">
							<?php echo $message; ?>
						</div>
					<?php endif; ?>

					<?php if($approval_alert) : ?>
						<div class="alert alert-danger" role="alert">
							<?php echo $message; ?>
						</div>
					<?php endif; ?>

          <!-- Page Heading -->
					<!-- <h1 class="h3 mb-2 text-gray-800">Senarai Tempahan Baru</h1><br> -->
					

						<!-- Content Row -->
						<div class="row">

							<!-- Aras 1 -->
							<div class="col-xl-3 col-md-6 mb-4">
								<a href="reservation_approval.php?weekdays">
									<div class="card border-left-primary shadow h-100 py-2">
										<div class="card-body">
											<div class="row no-gutters align-items-center">
												<div class="col mr-2">
													<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Hari Bekerja</div>
													<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div> -->
												</div>
												<div class="col-auto">
													<i class="fas fa-calendar-check fa-2x text-gray-300"></i>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>

							<!-- Aras 2 -->
							<div class="col-xl-3 col-md-6 mb-4">
								<a href="reservation_approval.php?weekends">
									<div class="card border-left-success shadow h-100 py-2">
										<div class="card-body">
											<div class="row no-gutters align-items-center">
												<div class="col mr-2">
													<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Hari Cuti</div>
													<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
												</div>
												<div class="col-auto">
													<i class="fas fa-calendar-times fa-2x text-gray-300"></i>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
						<br>

						<?php 

						if(isset($_GET['weekdays'])){

							$default = false;
							$query_reservation = "SELECT * FROM tempahan WHERE status = 'Pending' AND WEEKDAY(start_date) IN (0, 1, 2, 3, 6)";
							$result_reservation = mysqli_query($connection, $query_reservation);

							?>

							<!-- Weekday -->
							<!-- DataTales Example -->
							<h1 class="h3 mb-2 text-gray-800">Senarai Tempahan Hari Bekerja</h1>
							<div class="card shadow mb-4">
								<!-- <div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
								</div> -->
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Bil</th>
													<th>Nama Pelajar</th>
													<th>No Matrik</th>
													<th>Ruang</th>
													<th>Tujuan</th>
													<th>No Tel</th>
													<th>Tarikh Mula</th>
													<th>Tarikh Akhir</th>
													<th>Masa Mula</th>
													<th>Masa Akhir</th>
													<th>Sahkan</th>
													<th>Batalkan</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Bil</th>
													<th>Nama Pelajar</th>
													<th>No Matrik</th>
													<th>Ruang</th>
													<th>Tujuan</th>
													<th>No Tel</th>
													<th>Tarikh Mula</th>
													<th>Tarikh Akhir</th>
													<th>Masa Mula</th>
													<th>Masa Akhir</th>
													<th>Sahkan</th>
													<th>Batalkan</th>
												</tr>
											</tfoot>
											<tbody>
											
											<?php
												$i = 1;
												while($row_reservation = mysqli_fetch_assoc($result_reservation)) : ?>

													<tr>
													<?php

															$room_id = $row_reservation['room_id'];
															$query_room = "SELECT * FROM rooms WHERE id = $room_id";
															$result_room = mysqli_query($connection, $query_room);
															$row_room = mysqli_fetch_array($result_room);
													?>

														<td><?php echo $i; ?></td>
														<td><?php echo $row_reservation['name']; ?></td>
														<td><?php echo $row_reservation['user_matric_num']; ?></td>
														<td><?php echo $row_room['name']; ?></td>
														<td><?php echo $row_reservation['purposes']; ?></td>
														<td><?php echo $row_reservation['no_tel']; ?></td>
														<td><?php echo $row_reservation['start_date']; ?></td>
														<td><?php echo $row_reservation['end_date']; ?></td>
														<td><?php echo $row_reservation['start_time']; ?></td>
														<td><?php echo $row_reservation['end_time']; ?></td>
														<td><a onclick="return confirm('Anda pasti ingin membatalkan tempahan ini?')" href="reservation_approval.php?approve=<?php echo $row_reservation['id']; ?>" class="btn btn-success"><span ></span> Sah </a></td>
														<td><a onclick="return confirm('Anda pasti ingin membatalkan tempahan ini?')" href="reservation_approval.php?unapprove=<?php echo $row_reservation['id']; ?>" class="btn btn-danger"><span ></span> Batal </a></td>
													</tr>

													<?php $i++; ?>

												<?php endwhile; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						<?php
						}
						?>


						<?php  

						if(isset($_GET['weekends'])){

							$default = false;
							$query_reservation_holiday = "SELECT * FROM tempahan WHERE status = 'Pending' AND WEEKDAY(start_date) BETWEEN 4 AND 5";
							$result_reservation_holiday = mysqli_query($connection, $query_reservation_holiday);

							?>

							<h1 class="h3 mb-2 text-gray-800">Senarai Tempahan Hari Cuti</h1>
							<!-- Weekends -->
							<!-- DataTales Example -->
							<div class="card shadow mb-4">
								<!-- <div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
								</div> -->
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Bil</th>
													<th>Nama Pelajar</th>
													<th>No Matrik</th>
													<th>Ruang</th>
													<th>Tujuan</th>
													<th>No Tel</th>
													<th>Tarikh Mula</th>
													<th>Tarikh Akhir</th>
													<th>Masa Mula</th>
													<th>Masa Akhir</th>
													<th>Sahkan</th>
													<th>Batalkan</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Bil</th>
													<th>Nama Pelajar</th>
													<th>No Matrik</th>
													<th>Ruang</th>
													<th>Tujuan</th>
													<th>No Tel</th>
													<th>Tarikh Mula</th>
													<th>Tarikh Akhir</th>
													<th>Masa Mula</th>
													<th>Masa Akhir</th>
													<th>Sahkan</th>
													<th>Batalkan</th>
												</tr>
											</tfoot>
											<tbody>
											
											<?php
												$i = 1;
												while($row_reservation_holiday = mysqli_fetch_assoc($result_reservation_holiday)) : ?>

													<tr>
													<?php

															$room_id = $row_reservation_holiday['room_id'];
															$query_room = "SELECT * FROM rooms WHERE id = $room_id";
															$result_room = mysqli_query($connection, $query_room);
															$row_room = mysqli_fetch_array($result_room);
													?>

														<td><?php echo $i; ?></td>
														<td><?php echo $row_reservation_holiday['name']; ?></td>
														<td><?php echo $row_reservation_holiday['user_matric_num']; ?></td>
														<td><?php echo $row_room['name']; ?></td>
														<td><?php echo $row_reservation_holiday['purposes']; ?></td>
														<td><?php echo $row_reservation_holiday['no_tel']; ?></td>
														<td><?php echo $row_reservation_holiday['start_date']; ?></td>
														<td><?php echo $row_reservation_holiday['end_date']; ?></td>
														<td><?php echo $row_reservation_holiday['start_time']; ?></td>
														<td><?php echo $row_reservation_holiday['end_time']; ?></td>
														<td><a class="btn btn-success" href="assign_ja.php?approve=<?php echo $row_reservation_holiday['id']; ?>">Sah</a></td>
														<td><a href="reservation_approval.php?unapprove=<?php echo $row_reservation_holiday['id']; ?>" class="btn btn-danger">Batal</a></td>
													</tr>
													<?php $i++; ?>

												<?php endwhile; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						<?php
						}
						?>

						<?php
						if($default){
						?>
							<!-- Weekday -->
							<!-- DataTales Example -->
							<h1 class="h3 mb-2 text-gray-800">Senarai Tempahan Hari Bekerja</h1>
							<div class="card shadow mb-4">
								<!-- <div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
								</div> -->
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Bil</th>
													<th>Nama Pelajar</th>
													<th>No Matrik</th>
													<th>Ruang</th>
													<th>Tujuan</th>
													<th>No Tel</th>
													<th>Tarikh Mula</th>
													<th>Tarikh Akhir</th>
													<th>Masa Mula</th>
													<th>Masa Akhir</th>
													<th>Sahkan</th>
													<th>Batalkan</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Bil</th>
													<th>Nama Pelajar</th>
													<th>No Matrik</th>
													<th>Ruang</th>
													<th>Tujuan</th>
													<th>No Tel</th>
													<th>Tarikh Mula</th>
													<th>Tarikh Akhir</th>
													<th>Masa Mula</th>
													<th>Masa Akhir</th>
													<th>Sahkan</th>
													<th>Batalkan</th>
												</tr>
											</tfoot>
											<tbody>
											
											<?php
												$i = 1;
												while($row_reservation_default = mysqli_fetch_assoc($result_reservation_default)) : ?>

													<tr>
													<?php

															$room_id = $row_reservation_default['room_id'];
															$query_room = "SELECT * FROM rooms WHERE id = $room_id";
															$result_room = mysqli_query($connection, $query_room);
															$row_room = mysqli_fetch_array($result_room);
													?>

														<td><?php echo $i; ?></td>
														<td><?php echo $row_reservation_default['name']; ?></td>
														<td><?php echo $row_reservation_default['user_matric_num']; ?></td>
														<td><?php echo $row_room['name']; ?></td>
														<td><?php echo $row_reservation_default['purposes']; ?></td>
														<td><?php echo $row_reservation_default['no_tel']; ?></td>
														<td><?php echo $row_reservation_default['start_date']; ?></td>
														<td><?php echo $row_reservation_default['end_date']; ?></td>
														<td><?php echo $row_reservation_default['start_time']; ?></td>
														<td><?php echo $row_reservation_default['end_time']; ?></td>
														<td><a href="reservation_approval.php?approve=<?php echo $row_reservation_default['id']; ?>" class="btn btn-success">Sah</a></td>
														<td><a href="reservation_approval.php?unapprove=<?php echo $row_reservation_default['id']; ?>" class="btn btn-danger">Batal</a></td>
													</tr>

													<?php $i++; ?>

												<?php endwhile; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						<?php
						}
						?>
						
						
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->