<?php include "../includes/admin_header.php"; ?>
<?php $approval_success = false; ?>
<?php $approval_alert = false; ?>

<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

?>

<?php 

if(isset($_GET['approve'])){

	$reservation_id_to_approve = $_GET['approve'];

	$query_to_display = "SELECT * FROM tempahan WHERE id = $reservation_id_to_approve";
	$result_to_display = mysqli_query($connection, $query_to_display);
	$row_to_display = mysqli_fetch_array($result_to_display);

	//query to display room name based on the room id
	$room_id_to_display = $row_to_display['room_id'];
	$query_room_to_display = "SELECT * FROM rooms WHERE id = $room_id_to_display";
	$result_room_to_display = mysqli_query($connection, $query_room_to_display);
	$row_room_to_display = mysqli_fetch_array($result_room_to_display);

}
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <center><h1 class="h3 mb-2 text-gray-800">Sila Pilih Penolong Jurutera Bertugas Sebelum Sahkan</h1></center>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

							<!-- Outer Row -->
							<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-12">
										<div class="p-5">
											<form class="user" method="post" action="reservation_approval.php">
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Pelajar</label>
													<input type="text" class="form-control form-control-user" name="room_name" value="<?php echo $row_to_display['name'];?>" disabled>
												</div>
												<div class="form-group">
													<!-- <label for="name" style="margin-left:18px">Tempahan ID</label> -->
													<input type="text" class="form-control form-control-user" name="reservation_id" value="<?php echo $row_to_display['id'];?>" hidden>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">No Matrik</label>
													<input type="text" class="form-control form-control-user" name="no_matric" value="<?php echo $row_to_display['user_matric_num']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Ruang</label>
													<input type="text" class="form-control form-control-user" name="room_name" value="<?php echo $row_room_to_display['name']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">No Telefon</label>
													<input type="text" class="form-control form-control-user" name="no_tel" value="<?php echo $row_to_display['no_tel']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tarikh Mula</label>
													<input type="text" class="form-control form-control-user" name="start_date" value="<?php echo $row_to_display['start_date']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tarikh Akhir</label>
													<input type="text" class="form-control form-control-user" name="end_date" value="<?php echo $row_to_display['end_date']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Masa Mula</label>
													<input type="text" class="form-control form-control-user" name="start_date" value="<?php echo $row_to_display['start_time']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Masa Akhir</label>
													<input type="text" class="form-control form-control-user" name="end_date" value="<?php echo $row_to_display['end_time']; ?> " disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tujuan</label>
													<textarea rows="7" cols="50"  class="form-control" name="room_purpose" disabled><?php echo $row_to_display['purposes']; ?></textarea>
												</div>
												<div class="form-group">
													<label for="room_ja_id">Sila Pilih Penolong Jurutera Bertugas</label>
													<select id="room_ja_id" class="form-control" name="room_ja_id" required>
														<option value="">-- Pilih --</option>
														<?php 
														$query_ja = "SELECT * FROM users WHERE user_role = \"Penolong Jurutera\"";
														$result_ja = mysqli_query($connection, $query_ja);
														while($row_ja = mysqli_fetch_array($result_ja)): ?>
															<option value="<?php echo $row_ja['user_id']; ?>" name="room_ja_id"><?php echo $row_ja['first_name'] . " " . $row_ja['last_name']; ?></option>
														<?php endwhile; ?>						
													</select>
												</div>

												<input type="submit" class="btn btn-success btn-user btn-block" name="submit" value="Sahkan">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>

					
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->