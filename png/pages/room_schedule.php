<?php include "../includes/admin_header.php"; ?>

<?php

	$room_id = $_GET['id'];

	if (isset($_POST['submit'])) {
		
		$subject = strtoupper(mysqli_real_escape_string($connection, $_POST['subject']));
		$section = mysqli_real_escape_string($connection, $_POST['section']);
		$lecturer = strtoupper(mysqli_real_escape_string($connection, $_POST['lecturer']));
		$programme = strtoupper(mysqli_real_escape_string($connection, $_POST['programme']));
		$day_schedule_id = mysqli_real_escape_string($connection, $_POST['day_schedule_id']);
		$start_time = mysqli_real_escape_string($connection, $_POST['start_time']);
		$end_time = mysqli_real_escape_string($connection, $_POST['end_time']);

		$query = "INSERT INTO schedules (start_time, end_time, subject, section, lecturer, programme, day_schedule_id, room_id) ";
		$query .= "VALUES ('$start_time', '$end_time', '$subject', '$section', '$lecturer', '$programme', $day_schedule_id, $room_id)";
		$result = mysqli_query($connection, $query);
		header("Location: room_schedule.php?id=$room_id");
	}
 ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <center><h1 class="h3 mb-4 text-gray-800">Jadual</h1></center>

					<div class="container">

							<!-- Outer Row -->
							<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-6">
												<a class="btn btn-primary btn-user btn-block" href="room_schedule_add.php?id=<?php echo $room_id;?>">Tambah Jadual Baru</a>
									</div>
									<div class="col-lg-6">
												<a class="btn btn-primary btn-user btn-block" href="schedule_list.php?id=<?php echo $room_id;?>">Senarai Jadual</a>
									</div>
								</div>
							</div>
						</div>

							<!-- Senarai Jadual -->
							<script>document.getElementsByTagName("html")[0].className += " js";</script>
							<link rel="stylesheet" href="schedule_assets/css/style.css">
							<div class="cd-schedule cd-schedule--loading margin-top-lg margin-bottom-lg js-cd-schedule">
								<div class="cd-schedule__timeline">
									<ul>
										<li><span>08:00</span></li>
										<li><span>08:30</span></li>
										<li><span>09:00</span></li>
										<li><span>09:30</span></li>
										<li><span>10:00</span></li>
										<li><span>10:30</span></li>
										<li><span>11:00</span></li>
										<li><span>11:30</span></li>
										<li><span>12:00</span></li>
										<li><span>12:30</span></li>
										<li><span>13:00</span></li>
										<li><span>13:30</span></li>
										<li><span>14:00</span></li>
										<li><span>14:30</span></li>
										<li><span>15:00</span></li>
										<li><span>15:30</span></li>
										<li><span>16:00</span></li>
										<li><span>16:30</span></li>
										<li><span>17:00</span></li>
									</ul>
								</div> <!-- .cd-schedule__timeline -->
							
								<div class="cd-schedule__events">
									<ul>

										<li class="cd-schedule__group">
											<div class="cd-schedule__top-info"><span>Ahad</span></div>
							
											<ul>

												<?php 
												$query = "SELECT * FROM schedules WHERE day_schedule_id = 1 AND room_id = $room_id ORDER BY start_time ASC";
												$result = mysqli_query($connection, $query);
												while($row = mysqli_fetch_array($result)){
												?>
												<li class="cd-schedule__event">
													<a data-start="<?php echo $row['start_time'];?>" data-end="<?php echo $row['end_time'];?>" data-event="event-1">
														<em class="cd-schedule__name"><?php echo $row['subject'];?></em>
														<em class="cd-schedule__name"><?php echo $row['programme'];?> S<?php echo $row['section'];?></em>
														<em class="cd-schedule__name"><?php echo $row['lecturer'];?></em>
													</a>
												</li>
												<?php
												}
												?>
											</ul>					
										</li>
							
										<li class="cd-schedule__group">
											<div class="cd-schedule__top-info"><span>Isnin</span></div>
							
											<ul>
												<?php 
												$query = "SELECT * FROM schedules WHERE day_schedule_id = 2 AND room_id = $room_id ORDER BY start_time ASC";
												$result = mysqli_query($connection, $query);
												while($row = mysqli_fetch_array($result)){
												?>
												<li class="cd-schedule__event">
													<a data-start="<?php echo $row['start_time'];?>" data-end="<?php echo $row['end_time'];?>" data-event="event-2" href="#0">
														<em class="cd-schedule__name"><?php echo $row['subject'];?></em>
														<em class="cd-schedule__name"><?php echo $row['programme'];?> S<?php echo $row['section'];?></em>
														<em class="cd-schedule__name"><?php echo $row['lecturer'];?></em>
													</a>
													</a>
												</li>
												<?php
												}
												?>
											</ul>
										</li>
							
										<li class="cd-schedule__group">
											<div class="cd-schedule__top-info"><span>Selasa</span></div>
							
											<ul>
											<?php 
												$query = "SELECT * FROM schedules WHERE day_schedule_id = 3 AND room_id = $room_id ORDER BY start_time ASC";
												$result = mysqli_query($connection, $query);
												while($row = mysqli_fetch_array($result)){
												?>
												<li class="cd-schedule__event">
													<a data-start="<?php echo $row['start_time'];?>" data-end="<?php echo $row['end_time'];?>" data-event="event-3" href="#0">
														<em class="cd-schedule__name"><?php echo $row['subject'];?></em>
														<em class="cd-schedule__name"><?php echo $row['programme'];?> S<?php echo $row['section'];?></em>
														<em class="cd-schedule__name"><?php echo $row['lecturer'];?></em>
													</a>
													</a>
												</li>
												<?php
												}
												?>
											</ul>
										</li>
							
										<li class="cd-schedule__group">
											<div class="cd-schedule__top-info"><span>Rabu</span></div>
							
											<ul>
											<?php 
												$query = "SELECT * FROM schedules WHERE day_schedule_id = 4 AND room_id = $room_id ORDER BY start_time ASC";
												$result = mysqli_query($connection, $query);
												while($row = mysqli_fetch_array($result)){
												?>
												<li class="cd-schedule__event">
													<a data-start="<?php echo $row['start_time'];?>" data-end="<?php echo $row['end_time'];?>" data-event="event-4" href="#0">
														<em class="cd-schedule__name"><?php echo $row['subject'];?></em>
														<em class="cd-schedule__name"><?php echo $row['programme'];?> S<?php echo $row['section'];?></em>
														<em class="cd-schedule__name"><?php echo $row['lecturer'];?></em>
													</a>
													</a>
												</li>
												<?php
												}
												?>
											</ul>
										</li>
							
										<li class="cd-schedule__group">
											<div class="cd-schedule__top-info"><span>Khamis</span></div>
							
											<ul>
											<?php 
												$query = "SELECT * FROM schedules WHERE day_schedule_id = 5 AND room_id = $room_id ORDER BY start_time ASC";
												$result = mysqli_query($connection, $query);
												while($row = mysqli_fetch_array($result)){
												?>
												<li class="cd-schedule__event">
													<a data-start="<?php echo $row['start_time'];?>" data-end="<?php echo $row['end_time'];?>" data-event="event-1" href="#0">
														<em class="cd-schedule__name"><?php echo $row['subject'];?></em>
														<em class="cd-schedule__name"><?php echo $row['programme'];?> S<?php echo $row['section'];?></em>
														<em class="cd-schedule__name"><?php echo $row['lecturer'];?></em>
													</a>
													</a>
												</li>
												<?php
												}
												?>
											</ul>
										</li>
										
									</ul>
								</div>
							
								<div class="cd-schedule-modal">
									<header class="cd-schedule-modal__header">
										<div class="cd-schedule-modal__content">
											<span class="cd-schedule-modal__date"></span>
											<h3 class="cd-schedule-modal__name"></h3>
										</div>
							
										<div class="cd-schedule-modal__header-bg"></div>
									</header>
							
									<div class="cd-schedule-modal__body">
										<div class="cd-schedule-modal__event-info"></div>
										<div class="cd-schedule-modal__body-bg"></div>
									</div>
							
									<a href="#0" class="cd-schedule-modal__close text-replace">Close</a>
								</div>
							
								<div class="cd-schedule__cover-layer"></div>
							</div> <!-- .cd-schedule -->

							<script src="schedule_assets/js/util.js"></script>
							<script src="schedule_assets/js/main.js"></script>

					</div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
			
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->