-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2019 at 07:54 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `asset_name` varchar(255) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `tarikh_akhir_jaminan` date NOT NULL,
  `tender_no` varchar(255) NOT NULL,
  `talian_helpdesk` varchar(255) NOT NULL,
  `no_siri` varchar(255) NOT NULL,
  `asset_description` varchar(255) NOT NULL,
  `asset_ja_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `asset_name`, `supplier_name`, `tarikh_akhir_jaminan`, `tender_no`, `talian_helpdesk`, `no_siri`, `asset_description`, `asset_ja_id`, `status`) VALUES
(6, 'Nikon D5600 DSLR 1', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'ergwergwerg', 16, 'Dalam Tempahan'),
(7, 'Nikon D5600 DSLR 2', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sddsdsds', 16, 'Penyelenggaraan'),
(8, 'Nikon D5600 DSLR 3', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'fevfwev', 16, 'Sedia'),
(9, 'Nikon D5600 DSLR 4', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sddsdsds', 16, 'Sedia'),
(10, 'Nikon D5600 DSLR 5', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sdvsd', 16, 'Sedia'),
(11, 'Nikon D5600 DSLR 6', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sddsdsds', 16, 'Sedia'),
(12, 'Nikon D5600 DSLR 7', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', '', 16, 'Sedia'),
(13, 'test 2', 'd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'dfefdwef', 16, 'Sedia'),
(14, 'test 1', 'Basenet Technology Sdn Bhd', '2019-12-03', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'dvsdv', 21, 'Sedia'),
(15, '2', 'v', '2019-12-19', 'dfsfdf', '412312', 'cdf', 'faf', 16, 'Sedia'),
(16, '1', 'd', '2019-12-19', 'dfsfdf', '07-4537292@7295', '562RQ02', 'qfdwfdwfd', 16, 'ooo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
