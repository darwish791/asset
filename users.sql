-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2019 at 02:56 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `no_matric` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `no_tel` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `no_matric`, `user_email`, `no_tel`, `user_password`, `user_role`) VALUES
(15, 'user', '1', 'user1', 'darwish791@gmail.com', '', '$2y$12$blwDSTayNfHTzUIVG7RYFObrZQITIEx0hXBT5u3RENPWgPp2ZSkLa', 'Pengguna'),
(16, 'ja', '1', 'ja1', 'darwish791@gmail.com', '', '$2y$12$RfgmNpQB0aB4lCCzejNB6eUrx94ic/7V.CzPXIGXYoejeRTsjcBgi', 'Penolong Jurutera'),
(17, 'png', '1', 'png1', 'darwish791@gmail.com', '', '$2y$12$w89Yey89C5Fer.5Ag.8qaedOhIVMWmb3pjyZOTDLL4KcT6YguAhMm', 'Pengurus'),
(21, 'syam', '2', 'ja2', 'ihtisyamasor97@gmail.com', '', '$2y$12$VMuj4CgHUYUH.jhF3gKyYev7dnm6lQ51oJrzkPaVRxS/Mc03IK.7y', 'Penolong Jurutera'),
(22, 'ja', '3', 'ja3', 'darwish791@gmail.com', '', '$2y$12$8ymk/Rdyn/mcNWivcLqS.eP6EYdtKTQayYMufo/mVE/WSWX4YqPGa', 'Penolong Jurutera'),
(23, 'user', '2', 'user2', 'darwish791@gmail.com', '', '$2y$12$YZE2.TkkyPMPhbAbrUb2ROJmfwGLsUaRS1gI/r6Cl5ztsZ.dC3yl.', 'Pengguna'),
(24, 'harris', 'ali', 'ai160113', 'ishar97.ir@gmail.com', '', '$2y$12$2X3qygvmsgec8ts1LdjP9.dE/AWvie80DoVpRo.gZqaC5ZauYTKRy', 'Pengguna'),
(26, 'Test', 'Ja', 'ai160118', 'ishar97.ir@gmail.com', '', '$2y$12$tGETEHcc2pXeywdGKBIMvutXfqFzz9UMKzc.eY/8jYrv4JXajAilm', 'Penolong Jurutera');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
