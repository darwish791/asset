-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2020 at 07:30 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `asset_name` varchar(255) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `tarikh_akhir_jaminan` date NOT NULL,
  `tender_no` varchar(255) NOT NULL,
  `talian_helpdesk` varchar(255) NOT NULL,
  `no_siri` varchar(255) NOT NULL,
  `asset_description` varchar(255) NOT NULL,
  `asset_ja_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `asset_name`, `supplier_name`, `tarikh_akhir_jaminan`, `tender_no`, `talian_helpdesk`, `no_siri`, `asset_description`, `asset_ja_id`, `status`) VALUES
(6, 'Nikon D5600 DSLR 1', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'ergwergwerg', 16, 'Dalam Tempahan'),
(7, 'Nikon D5600 DSLR 2', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sddsdsds', 16, 'Penyelenggaraan'),
(8, 'Nikon D5600 DSLR 3', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'fevfwev', 16, 'Dalam Tempahan'),
(9, 'Nikon D5600 DSLR 4', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sddsdsds', 16, 'Sedia'),
(10, 'Nikon D5600 DSLR 5', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sdvsd', 16, 'Sedia'),
(11, 'Nikon D5600 DSLR 6', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'sddsdsds', 16, 'Sedia'),
(12, 'Nikon D5600 DSLR 7', 'Basenet Technology Sdn Bhd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', '', 16, 'Sedia'),
(13, 'test 2', 'd', '2019-12-19', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'dfefdwef', 16, 'Sedia'),
(14, 'test 1', 'Basenet Technology Sdn Bhd', '2019-12-03', 'UTHM(P)04/008/2013', '07-4537292@7295', '562RQ02', 'dvsdv', 21, 'Sedia'),
(15, '2', 'v', '2019-12-19', 'dfsfdf', '412312', 'cdf', 'faf', 16, 'Sedia'),
(16, '1', 'd', '2019-12-19', 'dfsfdf', '07-4537292@7295', '562RQ02', 'qfdwfdwfd', 16, 'ooo');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `user_matric_num` varchar(255) NOT NULL,
  `name` text DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `user_matric_num`, `name`, `user_email`, `start`, `end`, `room_id`, `status`, `paid`) VALUES
(21, '', '', '', '2019-11-04 14:00:00', '2019-11-05 00:00:00', 6, 'Pending', 0),
(25, '', 'kk', '', '2019-11-22 00:00:00', '2019-11-23 00:00:00', 1, 'Approved', 0),
(30, '', 'jj', '', '2019-11-20 08:00:00', '2019-11-22 12:00:00', 7, 'Pending', 0),
(40, '', '', '', '2019-11-20 00:00:00', '2019-11-21 00:00:00', 2, 'Pending', 0),
(41, '', '', '', '2019-11-19 00:00:00', '2019-11-20 00:00:00', 4, 'Approved', NULL),
(45, '', '', '', '2019-11-19 00:00:00', '2019-11-20 00:00:00', 1, 'Pending', NULL),
(49, '', '', '', '2019-11-25 08:00:00', '2019-11-26 10:00:00', 11, 'Pending', NULL),
(50, '', '', '', '2019-11-25 13:00:00', '2019-11-26 14:00:00', 11, 'Pending', NULL),
(51, '', '', '', '2019-11-25 13:00:00', '2019-11-26 14:00:00', 10, 'Pending', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `room_description` varchar(255) NOT NULL,
  `room_level` varchar(255) NOT NULL,
  `room_ja_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `capacity`, `status`, `room_description`, `room_level`, `room_ja_id`) VALUES
(3, 'Room 3', 2, 'Ready', '0', '1', 0),
(4, 'Room 4', 4, 'Ready', '0', '1', 0),
(5, 'Room 5', 1, 'Ready', '0', '2', 0),
(8, 'Makmal Realiti Maya', 30, 'Ready', '', 'Aras Bawah', 16),
(9, 'Makmal Studio dan Animasi (SAVA)', 32, 'Ready', '', 'Aras Bawah', 26),
(10, 'Makmal Pengkomputeran Pintar', 32, 'Ready', '', 'Aras Bawah', 21),
(11, 'Makmal Grafik Animasi', 32, 'Ready', '', 'Aras Bawah', 21);

-- --------------------------------------------------------

--
-- Table structure for table `room_bckup`
--

CREATE TABLE `room_bckup` (
  `room_id` int(3) NOT NULL,
  `room_name` varchar(255) NOT NULL,
  `room_capacity` int(11) NOT NULL,
  `room_description` varchar(255) NOT NULL,
  `room_level` varchar(255) NOT NULL,
  `room_ja_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_bckup`
--

INSERT INTO `room_bckup` (`room_id`, `room_name`, `room_capacity`, `room_description`, `room_level`, `room_ja_id`) VALUES
(1, 'makmal 1', 100, 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat saepe, assumenda corporis qui quis beatae quasi cum earum esse dolore necessitatibus amet accusamus officiis, voluptatem sunt! Animi quis molestiae similique.', '3', 21),
(2, 'makmal 2', 100, 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat saepe, assumenda corporis qui quis beatae quasi cum earum esse dolore necessitatibus amet accusamus officiis, voluptatem sunt! Animi quis molestiae similique.', '2', 16),
(3, 'makmal 3', 100, 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat saepe, assumenda corporis qui quis beatae quasi cum earum esse dolore necessitatibus amet accusamus officiis, voluptatem sunt! Animi quis molestiae similique.', 'Aras Bawah', 16),
(4, 'makmal 4', 200, 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat saepe, assumenda corporis qui quis beatae quasi cum earum esse dolore necessitatibus amet accusamus officiis, voluptatem sunt! Animi quis molestiae similique.', '1', 21),
(5, 'makmal 5', 1000, 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat saepe, assumenda corporis qui quis beatae quasi cum earum esse dolore necessitatibus amet accusamus officiis, voluptatem sunt! Animi quis molestiae similique.', 'Aras Bawah', 21),
(6, 'makmal 6', 100, 'Bla bla bla', '3', 21);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `subject` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `lecturer` varchar(255) NOT NULL,
  `programme` varchar(255) NOT NULL,
  `day_schedule_id` int(11) NOT NULL,
  `room_id` int(3) NOT NULL,
  `status` varchar(255) NOT NULL,
  `reserver` varchar(255) NOT NULL,
  `no_matric` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `start_time`, `end_time`, `subject`, `section`, `lecturer`, `programme`, `day_schedule_id`, `room_id`, `status`, `reserver`, `no_matric`, `year`) VALUES
(14, '08:00:00', '10:00:00', 'BIC24044', '2', 'Rahayu', '1BIM', 2, 1, 'Approved', '', '0', ''),
(15, '08:00:00', '10:00:00', 'BIC24044', '2', 'Rahayu', '1BIM', 1, 1, 'Approved', '', '0', ''),
(16, '08:00:00', '10:00:00', 'bic', '2', 'Rahayu', '1bim', 2, 1, 'Approved', '', '0', ''),
(17, '08:00:00', '10:00:00', 'BIC', '2', 'RAHAYU', '1BIM', 2, 1, 'Approved', '', '0', ''),
(20, '08:00:00', '10:00:00', 'BIC', '2', 'EWFVWF', 'DCWD', 3, 2, 'Approved', '', '0', ''),
(21, '08:00:00', '10:00:00', 'BIC', '3', 'EWFVWF', 'DCWD', 5, 0, 'Approved', '', '0', ''),
(22, '08:00:00', '10:00:00', 'BIC', '4', 'DVSWD', '1BIM', 3, 3, 'Approved', '', '0', ''),
(25, '13:00:00', '15:00:00', 'TEST', '2', 'HARRIS', '3BIM', 1, 1, 'Approved', '', '0', ''),
(26, '14:00:00', '16:00:00', 'TEST LAGI', '3', 'HARRISW', '3BIM', 2, 1, 'Approved', '', '0', ''),
(27, '13:00:00', '15:00:00', 'TEST 4', '1', 'NORASRI', '3BIM', 1, 1, 'Approved', '', '0', ''),
(30, '14:00:00', '16:00:00', 'BIM30403', '3', 'HARLINA', '3BIM', 3, 1, 'Approved', '', '0', ''),
(31, '08:00:00', '10:00:00', 'BIM30603', '7', 'JEBON', '3BIM', 3, 1, 'Approved', '', '0', ''),
(32, '08:00:00', '10:00:00', 'BIM10303', '4', 'NORASRI', '1BIM', 1, 8, 'Approved', '', '0', ''),
(33, '08:00:00', '10:00:00', 'BIM10303', '4', 'NORASRI', '1BIM', 1, 8, 'Approved', '', '0', ''),
(34, '10:00:00', '12:00:00', 'BIC10404', '2', 'FIRDAUS', '1BIP', 2, 8, 'Approved', '', '0', ''),
(35, '09:00:00', '11:00:00', 'BIC20404', '4', 'RAHAYU', '1BIS', 3, 8, 'Approved', '', '0', ''),
(36, '13:00:00', '15:00:00', 'BICHHH', '12', 'FIRDAUSH', '3BIM', 1, 8, 'Pending', 'harris', 'ai160113', ''),
(37, '13:02:00', '13:55:00', 'BIV6767', '3', 'HUHU', '16', 1, 8, 'Pending', 'harris', 'ai160113', '2020'),
(38, '13:00:00', '15:00:00', 'BIC10202', '8', 'HARRIS', '3BIM', 1, 9, 'Unapproved', 'harris', 'ai160113', '2020');

-- --------------------------------------------------------

--
-- Table structure for table `tempahan`
--

CREATE TABLE `tempahan` (
  `id` int(11) NOT NULL,
  `user_matric_num` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `purposes` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `no_tel` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `room_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempahan`
--

INSERT INTO `tempahan` (`id`, `user_matric_num`, `name`, `purposes`, `user_email`, `no_tel`, `start_date`, `end_date`, `start_time`, `end_time`, `room_id`, `status`) VALUES
(3, 'user1', 'user', 'test', 'darwish791@gmail.com', '0145256', '2019-11-30', '2019-11-30', '13:00:00', '15:00:00', 8, 'Approved'),
(4, 'user1', 'user', 'test2', 'darwish791@gmail.com', '012458896', '2019-11-23', '2019-11-23', '13:00:00', '15:00:00', 8, 'Pending'),
(13, 'user1', 'user', 'huhu', 'darwish791@gmail.com', '01245786', '2019-12-17', '2019-12-20', '14:00:00', '16:00:00', 8, 'Approved'),
(14, 'user1', 'user', 'gugu', 'darwish791@gmail.com', '014789655', '2019-12-21', '2019-12-24', '13:00:00', '14:00:00', 8, 'Pending'),
(15, 'user1', 'user', 'rt', 'darwish791@gmail.com', '0179536769', '2019-12-20', '2019-12-27', '13:00:00', '15:00:00', 8, 'Pending'),
(16, 'user1', 'user', 'huli', 'darwish791@gmail.com', '0147856', '2019-12-27', '2019-12-28', '13:00:00', '15:00:00', 8, 'Pending'),
(17, 'user1', 'user', 'tyu', 'darwish791@gmail.com', '0179536769', '2019-12-13', '2019-12-21', '13:00:00', '15:00:00', 8, 'Pending'),
(27, 'ai160113', 'harris', 'polis', 'ishar97.ir@gmail.com', '0147896', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 9, 'Approved'),
(28, 'ai160113', 'harris', 'polis', 'ishar97.ir@gmail.com', '0147896', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 9, 'Pending'),
(29, 'ai160113', 'harris', 'polis', 'ishar97.ir@gmail.com', '0147896', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 9, 'Pending'),
(35, 'ai160113', 'harris', 'polis', 'ishar97.ir@gmail.com', '0147896', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 9, 'Pending'),
(36, 'ai160113', 'harris', 'polis', 'ishar97.ir@gmail.com', '0147896', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 9, 'Pending'),
(37, 'ai160113', 'harris', 'polis', 'ishar97.ir@gmail.com', '0147896', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 9, 'Pending'),
(38, 'user1', 'user', 'polis', 'darwish791@gmail.com', '01365895', '2019-12-03', '2019-12-03', '13:00:00', '15:00:00', 10, 'Pending'),
(39, 'user1', 'user', 'pppp', 'darwish791@gmail.com', '0147963253', '2019-12-21', '2019-12-21', '13:00:00', '18:00:00', 9, 'Pending'),
(40, 'ai160113', 'harris', 'testing', 'ishar97.ir@gmail.com', '012478965', '2019-12-04', '2019-12-04', '13:00:00', '15:00:00', 9, 'Approved'),
(41, 'user1', 'user', 'testing', 'darwish791@gmail.com', '012345698', '2019-12-04', '2019-12-04', '13:00:00', '16:00:00', 9, 'Approved'),
(42, 'user1', 'user', 'testing', 'darwish791@gmail.com', '014789656', '2019-12-04', '2019-12-04', '13:00:00', '15:00:00', 9, 'Pending'),
(43, 'user1', 'user', 'tetsing email', 'darwish791@gmail.com', '017456986', '2019-12-08', '2019-12-08', '13:00:00', '16:00:00', 9, 'Pending'),
(44, 'user1', 'user', 'testing email png', 'darwish791@gmail.com', '01478965', '2019-12-14', '2019-12-14', '13:00:00', '15:00:00', 9, 'Pending'),
(45, 'ai160113', 'harris', 'testing hari cuti', 'ishar97.ir@gmail.com', '017953684', '2019-12-14', '2019-12-14', '13:00:00', '15:00:00', 9, 'Pending'),
(46, 'user1', 'user', 'testing email', 'darwish791@gmail.com', '01236548', '2019-12-18', '2019-12-18', '13:00:00', '15:00:00', 9, 'Unapproved'),
(47, 'ai160113', 'harris', 'testing email', 'ishar97.ir@gmail.com', '01479656', '2019-12-18', '2019-12-18', '13:00:00', '15:00:00', 9, 'Approved'),
(48, 'ai160113', 'harris', 'kursus', 'ishar97.ir@gmail.com', '0195103940', '2019-12-24', '2019-12-24', '13:00:00', '15:00:00', 9, 'Approved'),
(49, 'ai160113', 'harris', 'kursus2', 'ishar97.ir@gmail.com', '0195103940', '2019-12-14', '2019-12-14', '14:00:00', '15:00:00', 9, 'Pending'),
(50, 'ai160113', 'harris', 'kursus3', 'ishar97.ir@gmail.com', '0195103940', '2019-12-24', '2019-12-24', '13:00:00', '15:00:00', 9, 'Pending'),
(51, 'ai160113', 'harris', 'kursus 44', 'ishar97.ir@gmail.com', '0195103940', '2020-01-07', '2020-01-07', '13:00:00', '15:00:00', 9, 'Approved'),
(53, 'ai160113', 'harris', 'kursus 3', 'ishar97.ir@gmail.com', '0195103940', '2020-01-05', '2020-01-05', '13:00:00', '15:00:00', 9, 'Approved'),
(54, 'ai160113', 'harris', 'kk', 'ishar97.ir@gmail.com', '0195103940', '2020-01-08', '2020-01-08', '12:00:00', '14:00:00', 9, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `tempahan_asset`
--

CREATE TABLE `tempahan_asset` (
  `id` int(3) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `no_matric` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `no_tel` varchar(255) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `asset_id` int(3) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempahan_asset`
--

INSERT INTO `tempahan_asset` (`id`, `user_name`, `no_matric`, `user_email`, `no_tel`, `purpose`, `start_date`, `end_date`, `asset_id`, `status`) VALUES
(1, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'wervwev', '2019-12-13', '2019-12-19', 8, 'Pending'),
(2, 'user', 'user1', 'darwish791@gmail.com', '343423', 'fssdfsdf', '2019-12-25', '2019-12-20', 8, 'Pending'),
(3, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'wvcwev', '2019-12-13', '2019-12-21', 8, 'Pending'),
(4, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'tangkap gambar', '2019-12-17', '2019-12-26', 9, 'Pending'),
(5, 'user', 'user1', 'darwish791@gmail.com', '1561165515', 'test tempah', '2019-12-20', '2019-12-12', 14, 'Pending'),
(6, 'user', 'user2', 'darwish791@gmail.com', '1561165515', 'nak test tempah', '2019-12-17', '2019-12-20', 11, 'Pending'),
(7, 'user', 'user1', 'darwish791@gmail.com', '01479696', 'huhu', '2019-12-18', '2019-12-18', 8, 'Pending'),
(8, 'harris', 'ai160113', 'ishar97.ir@gmail.com', '0195103940', 'harris test tempah', '2019-12-18', '2019-12-18', 8, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `no_matric` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `no_tel` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `no_matric`, `user_email`, `no_tel`, `user_password`, `user_role`) VALUES
(15, 'user', '1', 'user1', 'darwish791@gmail.com', '', '$2y$12$lP3JzDaw27CJyeVoc8cPbOxeCESAsN0TdlvjY2PlosjQK4zZxe5N2', 'Pengguna'),
(16, 'ja', '1', 'ja1', 'darwish791@gmail.com', '', '$2y$12$RfgmNpQB0aB4lCCzejNB6eUrx94ic/7V.CzPXIGXYoejeRTsjcBgi', 'Penolong Jurutera'),
(17, 'png', '1', 'png1', 'darwish791@gmail.com', '', '$2y$12$w89Yey89C5Fer.5Ag.8qaedOhIVMWmb3pjyZOTDLL4KcT6YguAhMm', 'Pengurus'),
(21, 'syam', '2', 'ja2', 'ihtisyamasor97@gmail.com', '', '$2y$12$VMuj4CgHUYUH.jhF3gKyYev7dnm6lQ51oJrzkPaVRxS/Mc03IK.7y', 'Penolong Jurutera'),
(22, 'ja', '3', 'ja3', 'darwish791@gmail.com', '', '$2y$12$8ymk/Rdyn/mcNWivcLqS.eP6EYdtKTQayYMufo/mVE/WSWX4YqPGa', 'Penolong Jurutera'),
(23, 'user', '2', 'user2', 'darwish791@gmail.com', '', '$2y$12$YZE2.TkkyPMPhbAbrUb2ROJmfwGLsUaRS1gI/r6Cl5ztsZ.dC3yl.', 'Pengguna'),
(24, 'harris', 'ali', 'ai160113', 'ishar97.ir@gmail.com', '0195103940', '$2y$12$2X3qygvmsgec8ts1LdjP9.dE/AWvie80DoVpRo.gZqaC5ZauYTKRy', 'Pengguna'),
(26, 'Test', 'Ja', 'ai160118', 'ishar97.ir@gmail.com', '', '$2y$12$tGETEHcc2pXeywdGKBIMvutXfqFzz9UMKzc.eY/8jYrv4JXajAilm', 'Penolong Jurutera');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_bckup`
--
ALTER TABLE `room_bckup`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tempahan`
--
ALTER TABLE `tempahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tempahan_asset`
--
ALTER TABLE `tempahan_asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `room_bckup`
--
ALTER TABLE `room_bckup`
  MODIFY `room_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tempahan`
--
ALTER TABLE `tempahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tempahan_asset`
--
ALTER TABLE `tempahan_asset`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
