<?php include "../includes/header.php"; ?> 

<body id="page-top">

<?php include "../includes/top_nav.php"; ?> 


  <!-- Masthead -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">UTHM</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-2">PUSAT DATA</p>
          <p class="text-white-75 font-weight-light mb-2">UNIT ICT FSKTM UTHM</p>
          <p class="text-white-75 font-weight-light mb-5">UTHM</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#carta">MAKLUMAT LANJUT</a>
        </div>
      </div>
    </div>
  </header>

  <!-- About Section -->
  <section class="page-section bg-primary" id="carta">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="text-white mt-0">Carta Organisasi</h2>
          <hr class="divider light my-4">
					<div class="row justify-content-center">
             <img src="../img/pusatdataVii.png" alt="Organisasi Pusat Data">
           </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Services Section -->
  <section class="page-section" id="perkhidmatan">
    <div class="container">
      <h2 class="text-center mt-0">Perkhidmatan</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-6 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i>
            <h3 class="h4 mb-2">Tempahan Makmal</h3>
            <p class="text-muted mb-0">Tempahan makmal boleh dibuat menerusi laman web ini.</p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-globe text-primary mb-4"></i>
            <h3 class="h4 mb-2">Tempahan Peralatan</h3>
            <p class="text-muted mb-0">Tempahan peralatan boleh dibuat menerusi laman ini.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Portfolio Section -->
  <section id="portfolio">
    <div class="container-fluid p-0">
      <div class="row no-gutters">
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="../img/portfolio/fullsize/10.jpg">
            <img class="img-fluid" src="../img/portfolio/thumbnails/10.jpg" alt="">
            <div class="portfolio-box-caption">
              <!-- <div class="project-category text-white-50">
              </div> -->
              <div class="project-name">
                Memorandum of Agreement (MOA) Signing Ceremony
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="../img/portfolio/fullsize/11.jpg">
            <img class="img-fluid" src="../img/portfolio/thumbnails/11.jpg" alt="">
            <div class="portfolio-box-caption">
              <!-- <div class="project-category text-white-50">
                Category
              </div> -->
              <div class="project-name">
                Program Mobiliti Berkredit Telko University Bandung
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="../img/portfolio/fullsize/12.jpg">
            <img class="img-fluid" src="../img/portfolio/thumbnails/12.jpg" alt="">
            <div class="portfolio-box-caption">
              <!-- <div class="project-category text-white-50">
                Category
              </div> -->
              <div class="project-name">
                Lawatan Dari Universiti Gadjah Mada, Indonesia 
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="../img/portfolio/fullsize/13.jpg">
            <img class="img-fluid" src="../img/portfolio/thumbnails/13.jpg" alt="">
            <div class="portfolio-box-caption">
              <!-- <div class="project-category text-white-50">
                Category
              </div> -->
              <div class="project-name">
                Majlis Persaraan dan Sambutan Hari Lahir Staff FSKTM
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="../img/portfolio/fullsize/14.png">
            <img class="img-fluid" src="../img/portfolio/thumbnails/14.png" alt="">
            <div class="portfolio-box-caption">
              <!-- <div class="project-category text-white-50">
                Category
              </div> -->
              <div class="project-name">
                Jawatankuasa Unit ICT UTHM
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="../img/portfolio/fullsize/6.jpg">
            <img class="img-fluid" src="../img/portfolio/thumbnails/6.jpg" alt="">
            <div class="portfolio-box-caption p-3">
              <!-- <div class="project-category text-white-50">
                Category
              </div> -->
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>

  <!-- Call to Action Section -->
  <section class="page-section bg-dark text-white">
    <div class="container text-center">
      <h2 class="mb-4">Ketahui Lebih Lanjut Tentang FSKTM</h2>
      <a class="btn btn-light btn-xl" href="http://fsktm.uthm.edu.my/v4/">Lihat Sekarang</a>
    </div>
  </section>

  <!-- Contact Section -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">Hubungi kami!</h2>
          <hr class="divider my-4">
          <p class="text-muted mb-5">Terdapat aduan yang ingin dikemukakan? Hubungi kami menerusi telefon atau emel!</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
          <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
          <div>+6074533780</div>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
          <!-- Make sure to change the email address in anchor text AND the link below! -->
          <a class="d-block" href="mailto:fsktm@uthm.edu.my">fsktm@uthm.edu.my</a>
        </div>
      </div>
    </div>
  </section>

<?php include "../includes/footer.php"; ?> 