<?php
require_once '_db.php';

$aras = isset($_POST['aras']) ? $_POST['aras'] : '0';

$stmt = $db->prepare("SELECT * FROM rooms WHERE room_level = :aras OR :aras = '0' ORDER BY name");
$stmt->bindParam(':aras', $aras); 
$stmt->execute();
$rooms = $stmt->fetchAll();

class Room {}

$result = array();

foreach($rooms as $room) {
  $r = new Room();
  $r->id = $room['id'];
  $r->name = $room['name'];
  $r->capacity = $room['capacity'];
  $r->status = $room['status'];
  $result[] = $r;
  
}

header('Content-Type: application/json');
echo json_encode($result);

?>
