<?php include "../includes/admin_header.php"; ?>

<?php 
										
	$query_asset = "SELECT * FROM assets WHERE status = 'Sedia'";
	$result_asset = mysqli_query($connection, $query_asset);
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		
	<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

					<div class="alert alert-danger" role="alert">
						<h6>SYARAT/PERATURAN PERMOHONAN</h6>
						<OL>
							<LI>Tempahan hendaklah dibuat sekurang-kurangnya 3 hari sebelum tarikh penggunaan peralatan makmal.</LI>
							<LI>Tempoh pinjaman maksimum 3 hari termasuk hari cuti.</LI>
							<LI>Permohonan dari Fakulti/Pusat/Bahagian luar hendaklah melalui ketua PTJ masing-masing.</LI>
						</OL>
					</div>

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Peralatan dan Aset</h1>
          </div>

				<!-- Table -->
				<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>Deskripsi</th>
                      <th>Status</th>
                      <th>Tempah</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Bil</th>
                      <th>Nama</th>
                      <th>Deskripsi</th>
                      <th>Status</th>
                      <th>Tempah</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_asset = mysqli_fetch_assoc($result_asset)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row_asset['asset_name'];  ?></td>
												<td><?php echo $row_asset['asset_description']; ?></td>
												<td><?php echo $row_asset['status']; ?></td>
												<td><a href="add_reservation_asset.php?id=<?php echo $row_asset['id']; ?>" class="btn btn-primary">Tempah</a></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>
				<!-- End Table -->



      </div>
      <!-- End of Main Content -->
			
			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
