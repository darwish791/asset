<?php include "../includes/admin_header.php"; ?>
<?php $approval_success = false; ?>
<?php $approval_alert = false; ?>

<?php

if (isset($_GET['id'])) {
		
		$room_id = $_GET['id'];
		$query_room = "SELECT * FROM rooms WHERE id = $room_id";
		$result_room = mysqli_query($connection, $query_room);
		$row = mysqli_fetch_array($result_room);
		$ja_id = $row['room_ja_id']; //assign ja id to query ja email

	}

?>

<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

?>

<?php

	if (isset($_POST['submit'])) {
		
		$subject = strtoupper(mysqli_real_escape_string($connection, $_POST['subject']));
		$section = mysqli_real_escape_string($connection, $_POST['section']);
		$lecturer = strtoupper(mysqli_real_escape_string($connection, $_POST['lecturer']));
		$programme = strtoupper(mysqli_real_escape_string($connection, $_POST['programme']));
		$year = strtoupper(mysqli_real_escape_string($connection, $_POST['year']));
		$day_schedule_id = mysqli_real_escape_string($connection, $_POST['day_schedule_id']);
		$start_time = mysqli_real_escape_string($connection, $_POST['start_time']);
		$end_time = mysqli_real_escape_string($connection, $_POST['end_time']);
		$matric_num = $_SESSION['no_matric'];
		$first_name = $_SESSION['first_name'];
		$email = $_SESSION['user_email'];

		//query user to get email
		$query_user = "SELECT * FROM users WHERE user_id = $ja_id";
		$result_user = mysqli_query($connection, $query_user);
		$row_user = mysqli_fetch_array($result_user);

		//query user to get email for pengurus
		$query_png = "SELECT * FROM users WHERE user_role = 'Pengurus'";
		$result_png = mysqli_query($connection, $query_png);
		$row_png = mysqli_fetch_array($result_png);

		//assign email ja
		$mailto = $row_user['user_email'];
		$mailto2 = $row_png['user_email'];

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'datacenteruthm@gmail.com';                     // SMTP username
			$mail->Password   = 'kendecostonaten';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
			$mail->SMTPSecure = 'ssl';        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
			$mail->Port       = 465;                                    // TCP port to connect to
			
			$mail->SMTOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
	
			//Recipients
			$mail->setFrom('datacenteruthm@gmail.com', 'Pusat Data UTHM');
			$mail->addAddress($mailto);     // Add a recipient
			$mail->addAddress($mailto2);     // letak alamat emel pengurus makmal
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');
	
			// // Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	
			// Content
			$mail->isHTML(true);                                // Set email format to HTML
			$mail->Subject = 'Tempahan Sepanjang Semester Baharu Untuk Diluluskan';
			$mail->Body    = 'Ruang: ' .$row['name']. '<br>Masa: ' .$start_time. ' hingga ' .$end_time. '<br>Penempah: ' .$first_name. '<br>Sila log masuk untuk meluluskan tempahan. <br>http://localhost/asset/users/includes/login.php';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	
			$mail->send();
			$approval_success = true;
			$message = "Tempahan Berjaya";
			$query = "INSERT INTO schedules (start_time, end_time, subject, section, lecturer, programme, day_schedule_id, room_id, status, reserver, no_matric, year) ";
			$query .= "VALUES ('$start_time', '$end_time', '$subject', '$section', '$lecturer', '$programme', $day_schedule_id, $room_id, 'Pending', '$first_name', '$matric_num', '$year')";
			$result = mysqli_query($connection, $query);
		} catch (Exception $e) {
			$approval_alert = true;
			$message = "Tempahan tidak berjaya. Sila cuba lagi. Terdapat ralat: {$mail->ErrorInfo}";
		}
		// header("Location: index.php");
	}
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

				<?php if($approval_success) : ?>
					<div class="alert alert-success" role="alert">
						<?php echo $message; ?>
					</div>
				<?php endif; ?>

				<?php if($approval_alert) : ?>
					<div class="alert alert-danger" role="alert">
						<?php echo $message; ?>
					</div>
				<?php endif; ?>

          <!-- Page Heading -->
					<div class="container">

						<!-- Outer Row -->
						<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-12">
										<div class="p-5">
											<center><h1 class="h3 mb-4 text-gray-800">Tambah Jadual Baharu</h1></center>
											<form class="user" method="post" action="">
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Subjek</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: BIC10204" name="subject" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Seksyen</label>
													<input type="number" class="form-control form-control-user" id="" placeholder="eg: 2" name="section" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Pensyarah</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: Rahayu" name="lecturer" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Program</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: 1BIM" name="programme" required>
												</div>
												<div class="form-group">
													<label for="day" style="margin-left:18px">Hari</label>
													<select class="form-control" id="day" name="day_schedule_id" required>
														<option value="">-- Pilih --</option>
														<option value="1">Ahad</option>
														<option value="2">Isnin</option>
														<option value="3">Selasa</option>
														<option value="4">Rabu</option>
														<option value="5">Khamis</option>
													</select>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tahun</label>
													<input type="text" class="form-control form-control-user" id="" placeholder="eg: 2020" name="year" required>
												</div>
												<div class="form-group">
												<label for="start-time" style="margin-left:18px">Masa mula (Sistem 24 - jam)</label>
													<div class="col-md-4 col-md-offset-4"></div>
														<div class="input-group clockpicker" data-autoclose="true">
															<input name="start_time" type="text" class="form-control" value="eg: 13:00" required>
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-time"></span>
															</span>
														</div>
												</div>

												<div class="form-group">
												<label for="start-time" style="margin-left:18px">Masa tamat (Sistem 24 - jam)</label>
													<div class="col-md-4 col-md-offset-4"></div>
														<div class="input-group clockpicker" data-autoclose="true">
															<input name="end_time" type="text" class="form-control" value="eg: 15:00" required>
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-time"></span>
															</span>
														</div>
												</div>

												<input type="submit" class="btn btn-primary btn-user btn-block" name="submit" value="Simpan">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
			
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

<!-- Clockpicker -->
<script type="text/javascript">
	$('.clockpicker').clockpicker();
</script>
<!-- End Clockpicker -->