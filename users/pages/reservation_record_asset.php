<?php include "../includes/admin_header.php"; ?>

<?php 

	$no_matric = $_SESSION['no_matric'];				
	$query_record = "SELECT * FROM tempahan_asset WHERE no_matric = '$no_matric'";
	$result_record = mysqli_query($connection, $query_record);

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		
	<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Rekod Tempahan Asset</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>

        <!-- /.container-fluid -->

				<!-- Table -->
				<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama Bilik</th>
                      <th>Tujuan</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
											<th>Bil</th>
                      <th>Nama Bilik</th>
                      <th>Tujuan</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Status</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_record = mysqli_fetch_assoc($result_record)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<?php
													
													$asset_id = $row_record['asset_id'];
													$query_asset = "SELECT * FROM assets WHERE id = $asset_id";
													$result_asset = mysqli_query($connection, $query_asset);
													$row_asset = mysqli_fetch_array($result_asset);

												?>
												<td><?php echo $row_asset['asset_name'];  ?></td>
												<td><?php echo $row_record['purpose']; ?></td>
												<td><?php echo $row_record['start_date']; ?></td>
												<td><?php echo $row_record['end_date']; ?></td>
												<td><?php echo $row_record['status']; ?></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>
				<!-- End Table -->


				</div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
			
			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
