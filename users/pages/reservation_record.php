<?php include "../includes/admin_header.php"; ?>

<?php 

	$matric_num = $_SESSION['no_matric'];				
	$query_record = "SELECT * FROM tempahan WHERE user_matric_num = '$matric_num'";
	$result_record = mysqli_query($connection, $query_record);

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		
	<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

				<div class="row">

					<div class="col-xl-3 col-md-6 mb-4">
						<a href="reservation_record_sem.php">
							<div class="card border-left-success shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Rekod Tempahan Semester</div>
											<!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
										</div>
										<div class="col-auto">
											<i class="fas fa-calendar-check fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
					</div>
					<br>

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Rekod Tempahan Ruang Mingguan</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>

        <!-- /.container-fluid -->

				<!-- Table -->
				<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <!-- <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bil</th>
                      <th>Nama Bilik</th>
                      <th>Tujuan</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Masa Mula</th>
                      <th>Masa Akhir</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
											<th>Bil</th>
                      <th>Nama Bilik</th>
                      <th>Tujuan</th>
                      <th>Tarikh Mula</th>
                      <th>Tarikh Akhir</th>
                      <th>Masa Mula</th>
                      <th>Masa Akhir</th>
                      <th>Status</th>
                    </tr>
                  </tfoot>
                  <tbody>
									
									<?php
										$i = 1;
										while($row_record = mysqli_fetch_assoc($result_record)) : ?>

											<tr>
												<td><?php echo $i; ?></td>
												<?php
													
													$room_id = $row_record['room_id'];
													$query_room = "SELECT * FROM rooms WHERE id = $room_id";
													$result_room = mysqli_query($connection, $query_room);
													$row_room = mysqli_fetch_array($result_room);

												?>
												<td><?php echo $row_room['name'];  ?></td>
												<td><?php echo $row_record['purposes']; ?></td>
												<td><?php echo $row_record['start_date']; ?></td>
												<td><?php echo $row_record['end_date']; ?></td>
												<td><?php echo $row_record['start_time']; ?></td>
												<td><?php echo $row_record['end_time']; ?></td>
												<td><?php echo $row_record['status']; ?></td>
											</tr>

											<?php $i++; ?>

										<?php endwhile; ?>
										
                  </tbody>
                </table>
              </div>
            </div>
          </div>
				<!-- End Table -->


				</div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
			
			<?php include "../includes/admin_footer.php"; ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
