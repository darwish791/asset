<?php include "../includes/admin_header.php"; ?>
<?php $approval_success = false; ?>
<?php $approval_alert = false; ?>

<?php

if (isset($_GET['id'])) {
		
		$asset_id = $_GET['id'];
		$query_asset = "SELECT * FROM assets WHERE id = $asset_id";
		$result_asset = mysqli_query($connection, $query_asset);
		$row_asset = mysqli_fetch_array($result_asset);
	}

?>

<?php
if (isset($_POST['submit'])) {

	$user_name = $_SESSION['first_name'] ;
	$no_matric = $_SESSION['no_matric'];
	$user_email = $_SESSION['user_email'];
	$no_tel = mysqli_real_escape_string($connection, $_SESSION['user_no_tel']);
	$purpose = mysqli_real_escape_string($connection, $_POST['purpose']);
	$start_date = mysqli_real_escape_string($connection, $_POST['start_date']);
	$end_date = mysqli_real_escape_string($connection, $_POST['end_date']);


	$query = "INSERT INTO tempahan_asset (user_name, no_matric, user_email, no_tel, purpose, start_date, end_date, asset_id, status) ";
	$query .= "VALUES ('$user_name', '$no_matric', '$user_email', '$no_tel', '$purpose', '$start_date', '$end_date', '$asset_id', 'Pending')";
	$result = mysqli_query($connection, $query);
	header("Location: reservation_record_asset.php");
}
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php include "../includes/admin_sidebar.php"; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include "../includes/admin_top_bar.php" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

					<div class="container">

					<!-- Page Heading -->
					<br><center><h1 class="h3 mb-4 text-gray-800">Tempah Aset</h1></center>

						<!-- Outer Row -->
						<div class="row justify-content-center">
							<div class="col-xl-10 col-lg-12 col-md-9">
								<!-- Nested Row within Card Body -->
								<div class="row">
									<div class="col-lg-12">
										<div class="p-5">
											<form class="user" method="post" action="">
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Aset</label>
													<input type="text" class="form-control form-control-user" name="asset_name" value="<?php echo $row_asset['asset_name'];?>" disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Nama Pemohon</label>
													<input type="text" class="form-control form-control-user" name="user_name" value="<?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name'];?>" disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">No Telefon Terkini</label>
													<input type="text" class="form-control form-control-user" name="no_tel" value="<?php echo $_SESSION['user_no_tel'];?>" disabled>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tujuan (Sila nyatakan)</label>
													<textarea rows="7" cols="50"  class="form-control" placeholder="Nyatakan tujuan..." name="purpose"></textarea>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tarikh Mula</label>
													<input type="date" class="form-control form-control-user" name="start_date" required>
												</div>
												<div class="form-group">
													<label for="name" style="margin-left:18px">Tarikh Akhir</label>
													<input type="date" class="form-control form-control-user" name="end_date" required>
												</div>

												<div class="alert alert-danger" role="alert">
													<h6>Saya dengan ini akan bertanggungjawab ke atas:</h6>
													<OL>
														<LI>Keselamatan peralatan yang akan dipinjam.</LI>
														<LI>Penggantian peralatan yang hilang atau rosak.</LI>
														<LI>Kebersihan peralatan semasa urusan pemulangan.</LI>
													</OL>
													<p><i>Pihak fakulti berhak untuk memberikan notis tuntutan gantirugi kepada pemohon sekiranya perkara diatas berlaku.</i></p>
												</div>

												<input type="submit" class="btn btn-primary btn-user btn-block" name="submit" value="Mohon">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

			<?php include "../includes/admin_footer.php"; ?>
			
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->