<?php 

include('config.php');
session_start();

	if(isset($_POST["submit"])) {

		$no_matric= mysqli_real_escape_string($connection, $_POST['no_matric']);
		$password= mysqli_real_escape_string($connection, $_POST['password']);

		$query=mysqli_query($connection,"SELECT * FROM users WHERE no_matric = '$no_matric'");
			
			while($row=mysqli_fetch_array($query)) {

				$db_first_name = $row['first_name'];
				$db_last_name = $row['last_name'];
				$db_no_matric = $row['no_matric'];
				$db_email = $row['user_email'];
				$db_no_tel = $row['no_tel'];
				$db_password = $row['user_password'];
				$db_role = $row['user_role'];
				$db_user_id = $row['user_id'];
		
		} 

		if(password_verify($password,$db_password))	{

					$_SESSION['first_name']=$db_first_name;
					$_SESSION['last_name']=$db_last_name;
					$_SESSION['no_matric']=$db_no_matric;
					$_SESSION['user_email']=$db_email;
					$_SESSION['user_no_tel']=$db_no_tel;
					$_SESSION['user_role']=$db_role;
					$_SESSION['user_password']=$db_password;
					$_SESSION['user_id']=$db_user_id;

					if ($_SESSION['user_role'] == "Pengguna") {
						header("Location: ../pages/room.php");
						exit();
					} elseif ($_SESSION['user_role'] == "Pengguna Belum Disahkan") {
						header("Location: ../pages/user_unapprove.php");
						exit();
					} elseif ($_SESSION['user_role'] == "Penolong Jurutera") {
						header("Location: ../../ja/pages/user.php");
						exit();
					} elseif ($_SESSION['user_role'] == "Penolong Jurutera Belum Disahkan") {
						header("Location: ../../ja/pages/ja_unapprove.php");
						exit();
					}elseif ($_SESSION['user_role'] == "Pengurus") {
						header("Location: ../../png/pages/user.php");
						exit();
					} 
				} else {
				header("Location: login.php");
				//echo("fail");
				}
	}
?>
