<?php include('config.php'); ?>


<?php

$message = "";

if (isset($_POST['register'])) {

	$first_name = mysqli_real_escape_string($connection, $_POST['first_name']);
	$last_name = mysqli_real_escape_string($connection, $_POST['last_name']);
	$no_matric = mysqli_real_escape_string($connection, $_POST['no_matric']);
	$email = mysqli_real_escape_string($connection, $_POST['email']);
	$no_tel = mysqli_real_escape_string($connection, $_POST['no_tel']);
	$password = mysqli_real_escape_string($connection, $_POST['password']);
	$repeat_password = mysqli_real_escape_string($connection, $_POST['repeat_password']);
	$user_role = mysqli_real_escape_string($connection, $_POST['user_role']);

	if ($password === $repeat_password) {
		$password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

		$query = "INSERT INTO users (first_name, last_name, no_matric, user_email, no_tel, user_password, user_role) ";
		$query .= "VALUES ('$first_name', '$last_name', '$no_matric', '$email', '$no_tel' '$password', '$user_role')";
		$result = mysqli_query($connection, $query);
		// header("Location: login.php");
		echo "<div class=\"alert alert-success\" role=\"alert\">Pendaftaran berjaya  <a href=\"login.php\" class=\"btn btn-primary\"	>Log masuk</a> </div>";
	} else {
		$message = "Kata Laluan tidak sepadan";
	}

 //<a href=\"#\">test<a/>

}

	?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Register</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">
	

</head>

<!-- <body class="bg-gradient-primary"> -->
<body background="../img/bg-masthead.jpg">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-6 d-none d-lg-block"><img src="../img/fsktm3.jpg" alt="" height="642px" width="560"></div>
          <div class="col-lg-6">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Daftar Akaun</h1>
              </div>
              <form class="user" action="" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="first_name" placeholder="Nama" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" name="last_name" placeholder="Nama" required>
                  </div>
                </div>
								<div class="form-group">
                  <input type="text" class="form-control form-control-user" name="no_matric" placeholder="No matrik" required>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" name="email" placeholder="Emel" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="no_tel" placeholder="No Telefon" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" name="password" placeholder="Kata laluan" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" name="repeat_password" placeholder="Ulang kata laluan" required>
                  </div>
                </div>
								<h6 class="text-center"><?php echo $message; ?></h6>
								<label for="pilihan">Jenis Pengguna</label>
								<select class="form-control" id="pilihan" name="user_role" required>
									<option value="Pengguna Belum Disahkan">Pelajar / Pensyarah</option>
									<option value="Penolong Jurutera Belum Disahkan">Penolong Jurutera</option>
								</select><hr>
                <input type="submit" class="btn btn-primary btn-user btn-block" name="register" value="Daftar">
							</form>
                <hr>
              <div class="text-center">
                <a class="small" href="login.php">Sudah mempunyai akaun? Log Masuk!</a><br>
                <a class="small" href="../pages/index.php">Kembali</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

</body>

</html>
