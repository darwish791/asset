<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
	<div class="container">
		<a class="navbar-brand js-scroll-trigger" href="#page-top">Pusat Data</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto my-2 my-lg-0">
				<li class="nav-item">
					<a class="nav-link js-scroll-trigger" href="#carta">Carta Organisasi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link js-scroll-trigger" href="#perkhidmatan">Perkhidmatan</a>
				</li>
				<li class="nav-item">
					<a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link js-scroll-trigger" href="#contact">Hubungi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link js-scroll-trigger" href="../users/includes/login.php">Log masuk</a>
				</li>
			</ul>
		</div>
	</div>
</nav>